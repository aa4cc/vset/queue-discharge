\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper, margin=3cm]{geometry}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{subcaption}
%\documentclass[11pt]{article}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[document]{ragged2e}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{fixltx2e}
\usepackage{hyperref}
\usepackage{textgreek}
\usepackage{multirow}
\usepackage{xspace}
\usepackage{natbib}

\title{Technical report for \\DATA-DRIVEN MODELING OF TRAFFIC QUEUE DISCHARGE DYNAMICS}
\author{Karolína Machová, Joonhong Min, Lukáš Pospíchal, Tomáš Zelinka}
%\date{}

\usepackage{natbib}
\usepackage{graphicx}

\begin{document}

\maketitle

\begin{abstract}
    This Technical report briefly approaches significant activities of the student project team related to data-driven modeling of traffic queue discharge dynamics. There is a chapter related to data collection of real data, data extraction, and data processing. The report is focused on the current state of scientific research of the microscopic traffic models and the possibility of their use for modeling of traffic queue discharge dynamics. It also provides a theoretical explanation of the one specific vehicle following model. The report includes a chapter related to parameter estimation of the mentioned model based on previously extracted data. This data fitting is compared to extracted data.
\end{abstract}

\section{Introduction}
The goal of the project is to develop a mathematical model or more models of dynamics of discharge of a queue of road vehicles at traffic-light controlled junctions in the reference of speed profiles and trajectories. The model should represent a real behaviour of car vehicles during crossing single  intersection controlled by the traffic lights and it will be used in later projects for feedback control algorithms design for traffic lights, in particular the preemption algorithms based on communication between emergency vehicles (ambulance, police, fire trucks) and the traffic lights controller. The model will be based on the data which will be recorded in real traffic. There is also the possibility to use vehicle-following models to fit the data.

\section{Data Collection}

We were unable to collect data ourselves, and as such, we asked for help the company DataFromSky. They provided us with their software, namely DataFromSky Viewer with two recordings of intersections. From this software, we extracted positions of vehicles in .csv file format, more precisely, positions of such vehicles, whose trajectories crossed the entry and exit gates. An example of one such extraction can be seen in Figure \ref{fig:inter}. This way, we were able to select only a subset of vehicles, e.g. cars going straight across the intersection.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\linewidth]{intersection.PNG}
    \caption{Intersection with an \textcolor{green}{entry gate} and an \textcolor{red}{exit gate}.}
    \label{fig:inter}
\end{figure}
Furthermore, We transformed the extracted positions to a coordinate frame of the entry gate. The left end of the entry gate, from the perspective of a vehicle, was set to be its origin, and the right was in the direction of the y-axis. The x-axis was constructed in such a way as to make the coordinates orthogonal.

Lastly, we computed velocities and accelerations of the individual vehicles using numerical differentiation and subsequently smoothing the data with a Savitzky-Golay filter to suppress the inaccuracies of the computation \cite{reff10}.

An example of an output of data extraction in the x-axis for the intersection mentioned above can be seen in Figure \ref{fig:extraction}.
\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{positions.eps}
        \caption{positions}
        \label{fig:pos}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{velocities.eps}
        \caption{velocities}
        \label{fig:vels}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{accelerations.eps}
        \caption{accelerations}
        \label{fig:accs}
    \end{subfigure}
    \caption{Extracted data [project team]}
    \label{fig:extraction}
\end{figure}


\section{Modeling of the Traffic Queue Discharge Dynamics}
\textbf{}
Extracted data were properly analysed and used for the data-driven modeling of traffic queue discharge dynamics. 
\subsection{Vehicle Following Models}
Microscopic models describe the traffic flow dynamics in terms of single vehicles. Vehicle following models are the most important mathematical representatives of microscopic traffic flow models. These models represent the traffic dynamics of individual driver-vehicle units. It means that driving behaviour depends not only on the driver but also on the acceleration and braking capabilities of the vehicle. Vehicle following models can describe all situations including acceleration and cruise in free traffic, following other vehicles in stationary and non-stationary cases, and approaching slow or standing vehicles, and red traffic lights \cite{reff3}.


Vehicle following models describe the behaviour of the individual driver-vehicle unit $\alpha$ in terms of dynamic variables: position $x_\alpha(t)$, speed $v_\alpha(t)$ and acceleration $\dot{v}_\alpha(t)$ Also, interactions with other vehicles and road segments are considered. Continuous-time model of driver response is governed by a set of equations \cite{reff5}:
\begin{equation}
    \dot{x}_\alpha(t)=v_\alpha(t)
\end{equation}

\begin{equation}
    \dot{v}_\alpha(t)=a\left(s_\alpha,v_\alpha,v_l\right)
\end{equation}
Where:\break
$s_\alpha$ - is the distance between vehicles (bumper-to-bumper distance),
$v_l$ - is the velocity of the leading vehicle.

There exist more types of car following models, e. g.: Optimal Velocity Model, Newell´s Car-Following Model, Gipps´ Model or Intelligent Driver Model (IDM).

\subsection{Intelligent Driver Model}
The Intelligent Driver Model (IDM) is the simplest complete and accident-free time-continuous model of acceleration profiles and a plausible behavior. IDM has specific properties described in \cite{reff3} and in \cite{ref1}.

The following equations describe the simplified version of the IDM, the dynamics of the individual driver-vehicle unit \cite{ref2}:

\begin{equation}
    \dot{x}_\alpha(t)=v_\alpha(t)
\end{equation}

\begin{equation}
    \dot{v}_\alpha(t)=a\left(1-\left(\frac{v_\alpha}{v_0}\right)^\delta-\left(\frac{s^*(v_\alpha,\Delta v_\alpha)}{s_\alpha}\right)^2\right)
\end{equation}
With:
\begin{equation}
    s^*\left(v_\alpha,\Delta v_\alpha \right)=s_0+v_\alpha T+\frac{v_\alpha \Delta v_\alpha}{2\sqrt{ab}}
\end{equation}
Where: \break
$v_0$ - desired velocity $[m/s]$;
$s_0$ - minimum bumper-to-bumper distance $[m]$;
$T$ - desired time headway $[s]$;
$a$ - maximum vehicle acceleration $[m/s^2]$;
$b$ - comfortable braking deceleration $[m/s^2]$;
$\delta$ - exponent (usually value is set to 4) $[-]$;
$\Delta v_\alpha$ - velocity difference (approaching rate) $[m/s]$;
$x_\alpha(t)$ - position of vehicle \textalpha\xspace $[m]$;
$l$ - car length $[m]$.

The acceleration of vehicle $\alpha$ can be separated info the following terms:\break
Free road term - leading vehicle:
\begin{equation}
    \dot{v}_\alpha(t)=a\left(1-\left(\frac{v_\alpha}{v_0}\right)^\delta\right)
\end{equation}
Interaction term – following vehicle:
\begin{equation}
    \dot{v}_\alpha(t)=-a\left(\left(\frac{s^*(V_\alpha,\Delta v_\alpha)}{s_\alpha}\right)^2\right)
\end{equation}
Definition of net distance is the following:
\begin{equation}
    s_\alpha=x_\alpha_-_1(t)-x_\alpha(t)-l_\alpha_-_1
\end{equation}
Where:\break
$x_\alpha_-_1(t)$ - position of the following vehicle $\alpha-1 [m]$;
$l_\alpha_-_1$ - length of the following vehicle $\alpha-1 [m]$;\break


Considering the previous set of equations and related input parameters for five vehicles in the traffic queue:\break
$l=3$ $m$

$v_0 = 11$ $m/s$

Figure \ref{fig:example} shows the output of the test scenario of traffic queue discharge dynamics.

There are some imperfections or disadvantages of the IDM for the modeling traffic queue discharge scenario \cite{ref2}:
\begin{itemize}
    \item The leading vehicle is starting with its maximum value of acceleration.
    \item Issues with keeping standing vehicles in a fixed position with zero velocity.
    \item The leading vehicles will not reach the desired velocity, and they approach that value.
\end{itemize}

The IDM is more accurately useful for modeling and simulating the \emph{steady-state} traffic situation, where there are no significant changes in acceleration, such as highway travel.

\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{idmp.png}
        \caption{Path profiles}
        \label{fig:pos2}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{idmv.png}
        \caption{Velocities profiles}
        \label{fig:vels2}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{idma.png}
        \caption{Accelerations profiles}
        \label{fig:accs2}
    \end{subfigure}
    \caption{Visualization of the traffic queue discharge scenario using the IDM [project team]}
    \label{fig:example}
\end{figure}

\subsection{Parameter Estimation and Data Fitting}
Considering the mentioned IDM and situation of the traffic queue discharge, it is needed to obtain values of the maximum acceleration and the desired time headway of each vehicle. Minimum bumper-to-bumper distance is considered as the actual bumper-to-bumper distance at the initial position of the vehicles, because during acceleration to the desired speed, the bumper-to-bumper range between vehicles is increasing. The mentioned facts are used for parameter estimation and data fitting.


The project team developed the \emph{MATLAB function} for IDM parameter estimation  called \emph{fitidm} designed for the traffic queue discharge situation \cite{reff4}. This function is including solving nonlinear curve-fitting (data-fitting) problems in the least-squares sense.


The mentioned \emph{MATLAB function-fitidm} was used for parameter estimation of extracted data. The input data and values were: extracted data - path profiles, velocity profiles, acceleration profiles, desired velocity ($v_0 = 11$ $m/s$) and length of each vehicle ($l = 3$ $m$). Example of the fitted extracted data to IDM using \emph{fitidm} can be seen in Figure \ref{fig:fitting} and related estimated parameters in Figure \ref{fig:mesh1}. Function \emph{fitidm} also provides  Coefficient of determination, which express the differences between user input data and IDM fitted data [Figure \ref{fig:mesh2}]. This measure is represented as a value between 0.0 (0.0 \%) and 1.0 (100 \%), where a value of 1.0 indicates a perfect fit.
\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{path_profiles.png}
        \caption{Path profiles}
        \label{fig:pos3}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{velocity_profiles.png}
        \caption{Velocities profiles}
        \label{fig:vels3}
    \end{subfigure}
    ~
    \begin{subfigure}[t]{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{acceleration_profiles.png}
        \caption{Accelerations profiles}
        \label{fig:accs3}
    \end{subfigure}
    \caption{Extracted Data (solid lines) vs IDM Data Fitting (dotted lines) [project team]}
    \label{fig:fitting}
\end{figure}


\begin{center}
    \begin{figure}
        \centering
        \includegraphics[scale=0.8]{est.JPG}
        \caption{Estimated parameters of the IDM. $Ai$ stands for maximum acceleration $[m/s^2]$ of the i-vehicle in row. Also, $Ti$ stands for desired time head-way $[s]$ of the i-vehicle in row [project team].}
        \label{fig:mesh1}
    \end{figure}
\end{center}

\begin{center}
    \begin{figure}
        \centering
        \includegraphics[scale=0.8]{coef.JPG}
        \caption{Coefficients of determination of the related data fitting. $Xi R2$ - coefficient of determination $[-]$ for difference in the path profiles for i-vehicle in row. $Vi R2$ - coefficient of determination $[-]$ for difference in the velocity profiles for i-vehicle in row  [project team].}
        \label{fig:mesh2}
    \end{figure}
\end{center}

\section{Discussion}
There is no specific way of classifying the car-following models. There is a way of classifying the models into heuristic, complete heuristic and behavioural models\cite{reff6}, or it is possible to divide them into analytical and Rule-based models\cite{reff12}.

There are abundant studies on modifying and improving existing car-following models, but only a handful of studies were found, dedicated to the vehicle discharging behaviour. Even there are, many studies were focusing on fast clearance of the queue than estimating the discharging time.

Yang, S\cite{reff7}, have optimized an IDM by a genetic algorithm to study the relation between queue discharging time and space in between the vehicles and Kovács et al.\cite{reff8} showed that it is possible to calibrate IDM with the proposed algorithm with additional parameters. On the other hand, Zhang et al.\cite{reff9} indicated that it is not possible to adequately model the behaviour of a driver in the vicinity of signalled intersections using IDM or FVDM, and suggested a new model improved from FVDM. Newell’s model is also verified, capable of describing the behaviour of discharging vehicles on signalled intersection\cite{reff11}. 

\section{Conclusion}
Despite the mentioned imperfections or disadvantages of the IDM (chapter 3.2), the results of the data fitting (using the parameter estimation of the IDM) of the extracted data to IDM are considered as satisfactory. For the path profiles were obtained positions in time with the high precision (more than 98 \%). In the case of velocity profiles, the IDM just provide \emph{smooth behaviour} also with high precision (more than 96 \%) except for the first vehicle. In this case, the IDM shows the mean values of velocity and related differences which are caused by the \emph{real-world} driver's reactions. However, the fitted data obtained for the IDM acceleration profiles had low precision, mainly caused by the IDM disadvantages. Using the IDM is considered as satisfactory for this data-driven modeling.

There are many possibilities for further studies:
\begin{itemize}
    \item Creation of the another data-driven model.
    \item Propose a real-time system for traffic lights for preemption based on the IDM.
    \item Realize video recording and image recognition for the data collection.
\end{itemize}

\bibliographystyle{unsrt}
\bibliography{references}
\end{document}