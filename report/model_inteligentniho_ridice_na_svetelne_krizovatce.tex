\documentclass[a4paper,11pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[czech]{babel}

\usepackage{fullpage}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{graphicx}
\usepackage{subcaption}

\usepackage[unicode]{hyperref}
\hypersetup{pdftitle={Validace modelu inteligentního řidiče na světelně řízené křižovatce pomocí dat z provozu}}
\hypersetup{pdfauthor={Zdeněk Hurák}}

\usepackage{siunitx}

%opening
\title{Validace modelu inteligentního řidiče na světelně řízené křižovatce pomocí dat z provozu}
\author{Zdeněk Hurák a Lukáš Pospíchal}

\begin{document}

\maketitle

\begin{abstract}
Ve zprávě dokumentujeme validaci jednoho konkrétního populárního matematického modelu dynamiky vozu (s lidským řidičem) -- tzv. \textit{modelu inteligentního řidiče} (angl. \textit{intelligent driver model, IDM}) -- v situaci, kdy se fronta vozů rozjíždí ``na zelenou'' na světelně řízené křižovatce. Motivací je nutnost tvorby matematických modelů vyprazdňování celé fronty (angl. queue discharge), na nichž budou postaveny algoritmy pro na modelu založené zpětnovazební řízení celé křižovatky. Z odborné literatury však neplyne přesvědčivě, že by IDM, který bývá typicky používán pro modelování provozu na dálnicích, spolehlivě vysvětloval/modeloval chování fronty vozidel při rozjezdu na křižovatce. Podstatou (pod)projektu bylo numerické hledání koeficientů, s kterými by model typu IDM vysvětloval naměřená data. 
\end{abstract}

\section{Úvod: motivace a cíl}
Existující řešení pro \textit{preempci} signálního plánu řadiče světelné křižovatky při průjezdu vozidla vozidla integrovaného záchranného systému (IZS) jsou založena na detekci vjezdu vozidla do dopředu zadané zóny (doslova do mapy nakresleného polygonu) okolo dané křižovatky. V ten okamžik je aktuální signální plán nahrazen sekvencí pro zajištění přednostního průjezdu vozidla IZS. Toto rešení zjevně nikterak nereaguje na aktuální situaci na křižovatce a ani nevyužívá jinou informaci od přijíždějícího vozidla IZS, než že právě protlo pomyslou hranici polygonální zóny. V článku \cite{obrusnik_queue_2020} jsme navrhli řešení, které využívá V2I komunikaci mezi přijíždějícím vozidlem IZS a řadičem křižovatky a využívá i znalost počtu vozů čekajících na křižovatce ve frontě na zelenou ve směru příjezdu vozidla IZS. Navržený algoritmus pak posouvá okamžik preempce signálního plánu v závislosti na znalosti počtu vozidel ve frontě i informací od vozidla IZS (pozice, rychlost, případně další), a snaží se tak minimalizovat dopady preempce na ostatní dopravu. 

Klíčovou složkou tohoto přístupu je kromě V2I komunikace a znalosti počtu vozidel ve frontě (získané například pomocí rozpoznávání obrazu) i model dynamiky fronty aut rozjíždějících se na křižovatce na zelenou -- jde o zpětnovazební řízení založené na modelu. Ve studii \cite{obrusnik_queue_2020} jsme pro modelování dynamiky jednotlivého vozidla ve frontě využívali populárního \textit{Modelu Inteligentního Řidiče} (angl. Intelligent Driver Model, IDM), který kromě dynamiky samotného vozidla parametrizuje právě i reakci řidiče na vozidlo před ním. Model byl navržen \cite{treiber_congested_2000} a popsán je v učebnicích jako například \cite{treiber_traffic_2013} (kapitola o IDM je dokonce volně ke stažení na \url{http://www.traffic-flow-dynamics.org/res/SampleChapter11.pdf}) či přehledových článcích jako \cite{wilson_car-following_2011}. I přes rozšířenost tohoto modelu v literatuře jsme nenalezli nikde potvrzení, že je model skutečně vhodný i pro námi řešenou situaci rozjezdu vozidel na křižovatce. Místo toho naprostá většina odborných prací využívá IDM pro modelování kolony vozidel v ustáleném stavu na dálnicích.  

I přestože zakomponování modelu IDM do zpětnovazební regulace popsané v naší studii \cite{obrusnik_queue_2020} vedlo k velmi přesvědčivým výsledkům, nebyli jsme si jistí, nakolik je dobrá odezva dána vhodností a kvalitou modelu a nakolik pouze inherentní robustností samotné zpětné vazby. Ve druhém případě by mohl být použit výrazně jednodušší model (s méně parametry a ideálně lineární). Proto jsme se rozhodli ověřit, zda model IDM dokáže skutečně dostatečně přesně modelovat rozjezd fronty vozidel při opouštění křižovatky na zelenou. Toto jsme se rozhodli realizovat numerickým hledáním koeficientů modelu IDM, které by vysvětlovaly empirická data z provozu. 

\section{Data z křižovatky}
Po počátečních záměrech získat trajektorie individuálních vozů při průjezdu zvolenou křižovatkou vlastním měřením, jsme nakonec oslovili brněnskou \href{https://www.rcesystems.cz/}{RCE systems s.r.o.}, která stojí za projektem \href{https://datafromsky.com/}{DataFromSky}, a od ní jsme dostali menší datovou sadu. Ta byla (jmenovanou firmou) získána extrakcí souřadnic vozidel z videa natočeného dronem visícím po nějakou dobu nad křižovatkou z Obr.~\ref{fig:pohled_na_krizovatku_z_dronu}. 

\begin{figure}[ht]
 \begin{subfigure}[b]{0.5\linewidth}
  \centering
  \includegraphics[width=7cm]{../raw_data/CR1_WITH_GATES_SNAPSHOT.png}
  \caption{Doplněny ``gates'' pro volby trajektorií}
 \end{subfigure}
 \begin{subfigure}[b]{0.5\linewidth}
  \centering
  \includegraphics[width=7cm]{../raw_data/CR1_WITH_POSITIONS_SNAPSHOT.png}
  \caption{Doplněny vypočítané souřadnice}
 \end{subfigure}
 \caption{Pohled na křižovatku z dronu (poskytnuto projektem DataFromSky)}
 \label{fig:pohled_na_krizovatku_z_dronu}
\end{figure}

Naměřené trajektorie z jednoho pruhu jsou vizualizovány v Obr.~\ref{fig:trajektorie_od_DataFromSky}. Zjevně jsou zahrnuty dva cykly světelné signalizace. V prvním cyklu čeká na zelenou celkem 13 vozidel, ve druhém jen 3.
\begin{figure}[ht]
 \centering
 \includegraphics[width=16cm]{../pictures/visualize_lane/I_2_L_17_POS.png}
 \caption{Naměřené trajektorie z jednoho (neodbočujícího) pruhu}
 \label{fig:trajektorie_od_DataFromSky}
\end{figure}

Současně s trajektoriemi jednotlivých vozů (tedy časovými průběhy jejich pozic) byly poskytnuty i z nich odhadnuté průběhy rychlostí. Ty jsme se však pro větší kontrolu nad celým procesem rozhodli počítat si sami. Pro numerický výpočet derivací jsme použili filtrační vyhlazovací algoritmus Savitzky-Golay \cite{schafer2011savitzky}. Průběhy rychlostí jsou v Obr.~\ref{fig:rychlosti_od_DataFromSky}.
\begin{figure}[ht]
 \centering
 \includegraphics[width=16cm]{../pictures/visualize_lane/I_2_L_17_VEL.png}
 \caption{Průběhy rychlostí odvozené z naměřených trajektorií z jednoho (neodbočujícího) pruhu}
 \label{fig:rychlosti_od_DataFromSky}
\end{figure}
% \begin{figure}[ht]
%  \centering
%  \includegraphics[width=16cm]{../pictures/visualize_lane/I_2_L_17_ACC.png}
%  \caption{Průběhy zrychlení odvozené z naměřených trajektorií z jednoho (neodbočujícího) pruhu}
%  \label{fig:zrychleni_od_DataFromSky}
% \end{figure}

\section{Model inteligentního řidiče}
Model byl navržen \cite{treiber_congested_2000} a popsán je v učebnicích jako například \cite{treiber_traffic_2013} (kapitola o IDM je dokonce volně ke stažení na \url{http://www.traffic-flow-dynamics.org/res/SampleChapter11.pdf}) či přehledových článcích jako \cite{wilson_car-following_2011}. Je vyjádřen následující diferenciální rovnicí pro rychlost vozidla
\begin{equation}
 \dot v(t) = a\left[ 1-\left(\frac{v(t)}{v_0}\right)^\delta -\left(\frac{s^\ast(v,\Delta v)}{s}\right)^2 \right],
\end{equation}
kde $v(t)$ je vlastní rychlost vozidla, $\Delta v(t)$ je relativní rychlost k předchozímu vozidlu, $a$ je pro řidiče maximální komfortní zrychlení, $b$ maximální komfortní decelerace, $v_0$ je typická cílová rychlost, $s(t)$ je aktuální vzdálenost k předchozímu vozu a $s^\ast$ je požadovaná vzdálenost, která je dána
\begin{equation}
 s^\ast(v,\Delta v) = s_0 + \max\left(0,vT + \frac{v\Delta v}{2\sqrt{ab}} \right)
\end{equation}
a obsahuje jak složku odpovídající ustálenému stavu $s_0+vT$, tak i dynamický člen $v\Delta v/(2\sqrt{ab})$ odpovídající ``inteligentnímu'' brzdění. 

V literatuře jsou ještě publikována rozšíření modelu IDM, která činí simulaci realističtější v případech, kdy je na začátku manévru rychlost $v$ vozidla vyšší než ustálená rychlost $v_0$.  

Typické hodnoty uváděné pro provoz osobních vozidel ve městě jsou podle \cite{treiber_traffic_2013}

\begin{center}
\begin{tabular}[c]{ll}
Parametr & Typická hodnota v městském provozu\\
\hline\\
Požadovaná rychlost $v_0$ & 50 \si{\kilo\metre\per\hour} \\
Časová mezera $T$ & 1.0 \si{\second}\\
Minimální mezera $s_0$ & 2 \si{\metre}\\
Akcelerační exponent $\delta$ & 4\\
Akcelerace & 1.5 \si{\metre\per\square\second}\\
Komfortní decelerace & 1.5 \si{\metre\per\square\second}
\end{tabular}
\end{center}

\section{Numerické hledání koeficientů model IDM pro vysvětlení dat z provozu}
S využitím dat z provozu jsme úlohu nalezení konkrétního hodnot (upravení typických hodnot uváděných v literatuře pro danou křižovatku) formulovali jako úlohu \textit{nelineárních nejmenších čtverců} a řešili ji v Matlabu. Pro nalezené hodnoty jsme ověřovali přesnost modelu oproti naměřeným datům, viz Obr.~\ref{fig:trajektorie_simulace} a \ref{fig:rychlosti_simulace}.
\begin{figure}[ht]
 \centering
 \includegraphics[width=16cm]{../pictures/visualize_model/I_2_L_17_Q_1_IIDM_POS_CZ.png}
 \caption{Trajektorie získané simulací modelu IDM a srovnané s naměřenými trajektoriemi z jednoho (neodbočujícího) pruhu}
 \label{fig:trajektorie_simulace}
\end{figure}
\begin{figure}[ht]
 \centering
 \includegraphics[width=16cm]{../pictures/visualize_model/I_2_L_17_Q_1_IIDM_VEL_CZ.png}
 \caption{Průběhy rychlostí získané simulací modelu IDM a srovnané s naměřenými trajektoriemi z jednoho (neodbočujícího) pruhu}
 \label{fig:rychlosti_simulace}
\end{figure}

Z průběhů jsou zřejmé některé nedostatky modelu IDM:  
\begin{enumerate}
 \item Model předpokládá rozjezd víceméně konstantním zrychlením až na saturační rychlost. Reálné chování řidičů je však zjevně složitěji – odjezd vozidla (zejména pokud čeká v zadních pozicích fronty) z křižovatky je charakterizován střídajícími se úseky zrychlování a brzdění, u některých vozidel (v našem grafu dokonce u toho prvního) lze dokonce pozorovat i překmit v rychlosti.
 \item Další pozorovaný nesoulad je možno pozorovat na samém začátku simulace, kdy v simulacích u několika posledních vozidel lze pozorovat, že ještě i ve fázi, kdy mají stát ve frontě v rozestupech inicializovaných empirickými daty, se tato vozidla snaží ty rozestupy upravit tak, aby odpovídaly parametru identifikovaného modelu.
\end{enumerate}

Přes tyto nepřesnosti modelu nelze pominout výhodné vlastnosti modelu IDM, a sice pohodlnou fyzikální interpretaci jednotlivých parametrů i jeho rozšířenost nejen v literatuře ale i v simulačních balících. Hlavním argumentem obhajoby použití modelu IDM v naší aplikaci však je, nepotřebujeme velmi přesně modelovat skutečný průběh rychlostí jednotlivých vozidel, nýbrž s ohledem na algoritmus sepnutí preempce potřebujeme model pouze pro odhadnutí doby, za jakou bude už i poslední vozidlo ve frontě rozjeté na saturační (ustálené) rychlosti. V tomto případě lze z Obrázku 42 vidět, že model je pro tyto účely překvapivě dobře použitelný – v experimentálních datech vidíme, že model takový okamžik předvídá okolo 25. Sekundy, v realitě to bylo jen o pár sekund později. Připomeneme, že nafitovaný model používá stejné parametry pro všechna vozidla ve frontě, a tak je takováto nepřesnost vlastně i překvapivě dobrá a použití model IDM se jeví jako ospravedlnitelné.  

Samozřejmě však je taková prvotní kvantifikace predikční chyby odhadu nedostatečná. Rigoróznější analýza by vyžadovala provedení takové analýzu na větším množství naměřených dat. To může být námětem na další výzkum. Kupříkladu by bylo zajímavé a jistě užitečné studovat na základě dat z provozu, zda a jak se mění tyto průměrné parametry modelu inteligentního řidiče v průběhu pracovního dne. Jestliže se z pochopitelných důvodů nemůže algoritmus přizpůsobovat jednotlivým řidičům, mohl by se aspoň přizpůsobovat denní fázím, podle kterých se mění i průměrné chování řidičů.   

\section{Závěr}
Přes uvedené nepřesnosti modelu nelze pominout výhodné vlastnosti modelu IDM, a sice pohodlnou fyzikální interpretaci jednotlivých parametrů i jeho rozšířenost nejen v literatuře ale i v simulačních balících. Hlavním argumentem obhajoby použití modelu IDM v naší aplikaci však je, nepotřebujeme velmi přesně modelovat skutečný průběh rychlostí jednotlivých vozidel, nýbrž s ohledem na algoritmus sepnutí preempce potřebujeme model pouze pro odhadnutí doby, za jakou bude už i poslední vozidlo ve frontě rozjeté na saturační (ustálené) rychlosti. V tomto případě lze z Obrázku 42 vidět, že model je pro tyto účely překvapivě dobře použitelný – v experimentálních datech vidíme, že model takový okamžik předvídá okolo 25. Sekundy, v realitě to bylo jen o pár sekund později. Připomeneme, že nafitovaný model používá stejné parametry pro všechna vozidla ve frontě, a tak je takováto nepřesnost vlastně i překvapivě dobrá a použití model IDM se jeví jako ospravedlnitelné.  

Samozřejmě však je taková prvotní kvantifikace predikční chyby odhadu nedostatečná. Rigoróznější analýza by vyžadovala provedení takové analýzu na větším množství naměřených dat. To může být námětem na další výzkum. Kupříkladu by bylo zajímavé a jistě užitečné studovat na základě dat z provozu, zda a jak se mění tyto průměrné parametry modelu inteligentního řidiče v průběhu pracovního dne. Jestliže se z pochopitelných důvodů nemůže algoritmus přizpůsobovat jednotlivým řidičům, mohl by se aspoň přizpůsobovat denní fázím, podle kterých se mění i průměrné chování řidičů.

\bibliographystyle{plain}
\bibliography{vehicular_platooning_CACC_ACC,vehicular_communication_networks,control_intersections_high-priority_traffic,car-following_model,filtering_estimation}



\end{document}
