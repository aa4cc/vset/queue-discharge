%% Script to test the feasibility of a simple second order LTI
%

format long;

MODEL = "LTI"; %choose model - IDM or IIDM
INTERSECTION = 2; %choose intersection 
LANE = 17;        %choose lane
STOP_TIME = 5;    %how long a car needs to go very slowly to be considered a part of a queue

A = read_data(INTERSECTION);            %reads raw data
lane = get_lane(A, INTERSECTION, LANE); %extracts only cars from selected lane
lane = filter_lane(lane, STOP_TIME);    %filters cars that are part of a queue
queues = split_queues(lane);            %splits queues so they can be analyzed separately
queues_len = length(queues);            %holds the number of queues extracted

sim_qs = cell(1,queues_len);            %preallocates a cell for the simulated queues 

%%
v_0 = 14;
a = 1;
delay = 1;

queue = queues{1};
n = length(queue);

A = [zeros(n), eye(n); zeros(n), -a*eye(n)];  
B = [zeros(n); eye(n)];
C = eye(2*n);
D = zeros(2*n,n);

sys = ss(A,B,C,D);

t0 = queue{1}.T(1);
t1 = queue{n}.T(end-1);
t = t0:0.04:t1;

u = zeros(length(t),n);

x_0 = zeros(2*n,1);
for vhcl = 1:n
    x_0(vhcl) = -queue{vhcl}.X(1);
    %x_0(n+vhcl) = queue{vhcl}.V(1);
    u((t-t0)>(vhcl-1)*delay, vhcl) = v_0;
end

y = lsim(sys, u, t-t0, x_0);



min_X = 0;
max_X = 0;
min_V = 0;
max_V = 0;

queue_len = length(queue);
figure(1);
clf;
subplot(2,1,1);
hold on;
for vhcl=1:queue_len
    if max(-queue{vhcl}.X) > max_X
        max_X = max(-queue{vhcl}.X);
    end
    if min(-queue{vhcl}.X) < min_X
        min_X = min(-queue{vhcl}.X);
    end
    
    
    p1 = plot(queue{vhcl}.T, -queue{vhcl}.X, '--');
    p2 = plot(t, y(:,vhcl));
    p2.Color = p1.Color;
end
hold off;
legend('real car', 'simulated car');
grid;
%xlim([queue{1}.T(1),queue{end}.T(end)]);
ylim([min_X,max_X])
xlabel('t [s]');
ylabel('x [m]');
title("Queue" + int2str(1));
subplot(2,1,2);
hold on;
for vhcl=1:queue_len
    if max(queue{vhcl}.V) > max_V
        max_V = max(queue{vhcl}.V);
    end
    if min(queue{vhcl}.V) < min_V
        min_V = min(queue{vhcl}.V);
    end
    p3 = plot(queue{vhcl}.T, queue{vhcl}.V, '--');
    p4 = plot(t, y(:,vhcl+queue_len));
    p4.Color = p3.Color;
end
hold off;
legend('real car', 'simulated car');
grid;
%xlim([queue{1}.T(1),queue{end}.T(end)]);
%ylim([min_V,max_V])
xlabel('t [s]');
ylabel('v [m/s]');
title("Queue" + int2str(1));







