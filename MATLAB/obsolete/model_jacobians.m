%% IDM - JACOBIANS CANNOT BE COMPUTED (I THINK)
syms v_0 a b l T s_0 delta v_l x_l v x real;

s = x_l - x - l;

% s_star = s_0 + max(0, v*T+v*(v-v_l)/(2*sqrt(a*b)));
% thus choose one:

s_star = s_0 + v*T+v*(v-v_l)/(2*sqrt(a*b)); % s_star > s_0
%s_star = s_0;                               % s_star = 0

acc = a*(1-(v/v_0)^delta - (s_star/s)^2);

f = [v; acc];
params = [v_0,T,a,b,l,s_0,delta];
A(v_0, T, a, b, l, s_0, delta, v_l, x_l, v, x) = jacobian(f, params);
f = matlabFunction(A)

%% IIDM
syms v_0 a b l T s_0 delta v_l x_l v x real;

s = x_l - x - l;
s_star = s_0 + max(0, x(2)*T+x(2)*dv/(2*sqrt(a*b)));


z = s_star/s;
if v <= v_0
    a_free = a*(1-(x(2)/v_0)^delta);
    if z >= 1
        acc = a*(1-z^2);
    else
        acc = a_free*(1-z^(2*a/a_free));
    end
else
    a_free = -b*(1-(v_0/x(2))^(a*delta/b));
    if z>= 1
        acc = a_free + a*(1-z^2);
    else
        acc = a_free;
    end
end