function [A]=fitidm(time,path_profiles,velocity_profiles,acceleration_profiles,desired_velocity,car_length)
%
%Intedned using only for study or scientific research purposes
%email: zelinto1@fel.cvut.cz
%2020
%
%PARAMTER ESTIMATION FOR IDM (for 5 vehicles)
%
%INPUT: 
%time: time vector, size: MX1 [s]
%
%path_profiles: matrix of path profiles, each column - vector for each vehicle,
%in order
%size: MX5 [m]
%
%velocity_profiles: matrix of velocity profiles, each column - vector for each vehicle,
%in order
%size: MX5 [m/s]
%acceleration_profiles: matrix of acceleration profiles, each column - vector for each vehicle, 
%in order
%size: MX5 [m/s]
%
%desired_velocity: 1X1 [m/s]
%
%car_length: vector of length of each vehicle: [1st;2nd;3rd;4th]
% size: 4X1 [m]
%
%EXAMPLE INPUT:
%A=fitidm(time,path,velocity,acceleration,11,car);
%OUTPUT:
%A=[A1,A2,T2,A3,T3,A4,T4,A5,T5]
%A1 - maximum accceleration of the 1st vehicle [m/s^2]
%A2 - maximum accceleration of the 2nd vehicle [m/s^2]
%T2 - desired time headway for 2nd vehicle [s]
%A3 - maximum accceleration of the 3rd vehicle [m/s^2]
%T3 - desired time headway for 3rd vehicle [s]
%A4 - maximum accceleration of the 4th vehicle [m/s^2]
%T4 - desired time headway for 4th vehicle [s]
%A5 - maximum accceleration of the 5th vehicle [m/s^2]
%T5 - desired time headway for 5th vehicle [s]
%
% see related documentation for further details
%
[u,~]=size(path_profiles);
ref1=path_profiles(1,1);

in1=(ref1==path_profiles(1:end,1));
init1=sum(in1) ;

out1=isnan(path_profiles(1:end,1));
end1=u-sum(out1);

    function S=fit11(A1,t)
        x0=[path_profiles(1,1),0];
        [~,Sv]=ode45(@DifEq1,t,x0);
        function dS=DifEq1(~,x)
            xdot=zeros(2,1);
            xdot(1)=x(2);
            xdot(2)=(A1(1).*(1-(x(2)./desired_velocity).^4));
            dS=xdot;
        end
        S=Sv(:,:);
    end

A1=0;
datax=[path_profiles(init1:end1,1),velocity_profiles(init1:end1,1)];
[A11] = lsqcurvefit(@fit11,A1,time(init1:end1,1),datax,0.5,3);

dif1=path_profiles(1,1)-path_profiles(1,2)-car_length(1,1);
out2=isnan(path_profiles(1:end,2));
end2=u-sum(out2);
 
    function S=fit12(A2,t)
        x0=[path_profiles(1,1),0,path_profiles(1,2),0];
        [~,Sv]=ode45(@DifEq2,t,x0);
        function dS=DifEq2(~,x)
            xdot=zeros(4,1);
            xdot(1)=x(2);
            xdot(2)=(A11.*(1-(x(2)./desired_velocity).^4));
            xdot(3)=x(4);
            xdot(4)=A2(1).*((1-(x(4)./desired_velocity).^4)-((dif1+(desired_velocity.*A2(2))+((desired_velocity.*(x(4)-x(2)))./(2.*sqrt(A2(1).*1.5))))./(x(1)-x(3))).^2);
            dS=xdot;
        end
        S=Sv(:,3:4);
    end

datax=[path_profiles(init1:end2,2),velocity_profiles(init1:end2,2)];
A2=[0,0];
[A12] = lsqcurvefit(@fit12,A2,time(init1:end2,1),datax,[0.5,0],[3,3]);

dif2=path_profiles(1,2)-path_profiles(1,3)-car_length(2,1);
out3=isnan(path_profiles(1:end,3));
end3=u-sum(out3);

    function S=fit13(A3,t)
        x0=[path_profiles(1,1),0,path_profiles(1,2),0,path_profiles(1,3),0];
        [~,Sv]=ode45(@DifEq3,t,x0);
        function dS=DifEq3(~,x)
            xdot=zeros(6,1);
            xdot(1)=x(2);
            xdot(2)=(A11.*(1-(x(2)./desired_velocity).^4));
            xdot(3)=x(4);
            xdot(4)=A12(1).*((1-(x(4)./desired_velocity).^4)-((dif1+(desired_velocity.*A12(2))+((desired_velocity.*(x(4)-x(2)))./(2.*sqrt(A12(1).*1.5))))./(x(1)-x(3))).^2);
            xdot(5)=x(6);
            xdot(6)=A3(1).*((1-(x(6)./desired_velocity).^4)-((dif2+(desired_velocity.*A3(2))+((desired_velocity.*(x(6)-x(4)))./(2.*sqrt(A3(1).*1.5))))./(x(3)-x(5))).^2);
           dS=xdot;
        end
        S=Sv(:,5:6);
    end
 
datax=[path_profiles(init1:end3,3),velocity_profiles(init1:end3,3)];%
A3=[0,0];
[A13] = lsqcurvefit(@fit13,A3,time(init1:end3,1),datax,[0.5,0],[3,3]);

dif3=path_profiles(1,3)-path_profiles(1,4)-car_length(3,1);
out4=isnan(path_profiles(1:end,4));
end4=u-sum(out4);
 
    function S=fit14(A4,t)
        x0=[path_profiles(1,1),0,path_profiles(1,2),0,path_profiles(1,3),0,path_profiles(1,4),0];
        [~,Sv]=ode45(@DifEq4,t,x0);
        function dS=DifEq4(~,x)
            xdot=zeros(8,1);
            xdot(1)=x(2);
            xdot(2)=(A11.*(1-(x(2)./desired_velocity).^4));
            xdot(3)=x(4);
            xdot(4)=A12(1).*((1-(x(4)./desired_velocity).^4)-((dif1+(desired_velocity.*A12(2))+((desired_velocity.*(x(4)-x(2)))./(2.*sqrt(A12(1).*1.5))))./(x(1)-x(3))).^2);
            xdot(5)=x(6);
            xdot(6)=A13(1).*((1-(x(6)./desired_velocity).^4)-((dif2+(desired_velocity.*A13(2))+((desired_velocity.*(x(6)-x(4)))./(2.*sqrt(A13(1).*1.5))))./(x(3)-x(5))).^2);
            xdot(7)=x(8);
            xdot(8)=A4(1).*((1-(x(8)./desired_velocity).^4)-((dif3+(desired_velocity.*A4(2))+((desired_velocity.*(x(8)-x(6)))./(2.*sqrt(A4(1).*1.5))))./(x(5)-x(7))).^2);
            dS=xdot;
        end
        S=Sv(:,7:8);
    end

datax=[path_profiles(init1:end4,4),velocity_profiles(init1:end4,4)];%
A4=[0,0];
[A14] = lsqcurvefit(@fit14,A4,time(init1:end4,1),datax,[0.5,0],[3,3]);

dif4=path_profiles(1,4)-path_profiles(1,5)-car_length(4,1);
out5=isnan(path_profiles(1:end,5));
end5=u-sum(out5);

    function S=fit15(A5,t)
        x0=[path_profiles(1,1),0,path_profiles(1,2),0,path_profiles(1,3),0,path_profiles(1,4),0,path_profiles(1,5),0];
        [~,Sv]=ode45(@DifEq5,t,x0);
        function dS=DifEq5(~,x)
            xdot=zeros(10,1);
            xdot(1)=x(2);
            xdot(2)=(A11.*(1-(x(2)./desired_velocity).^4));
            xdot(3)=x(4);
            xdot(4)=A12(1).*((1-(x(4)./desired_velocity).^4)-((dif1+(desired_velocity.*A12(2))+((desired_velocity.*(x(4)-x(2)))./(2.*sqrt(A12(1).*1.5))))./(x(1)-x(3))).^2);
            xdot(5)=x(6);
            xdot(6)=A13(1).*((1-(x(6)./desired_velocity).^4)-((dif2+(desired_velocity.*A13(2))+((desired_velocity.*(x(6)-x(4)))./(2.*sqrt(A13(1).*1.5))))./(x(3)-x(5))).^2);
            xdot(7)=x(8);
            xdot(8)=A14(1).*((1-(x(8)./desired_velocity).^4)-((dif3+(desired_velocity.*A14(2))+((desired_velocity.*(x(8)-x(6)))./(2.*sqrt(A14(1).*1.5))))./(x(5)-x(7))).^2);
            xdot(9)=x(10);
            xdot(10)=A5(1).*((1-(x(10)./desired_velocity).^4)-((dif4+(desired_velocity.*A5(2))+((desired_velocity.*(x(10)-x(8)))./(2.*sqrt(A5(1).*1.5))))./(x(7)-x(9))).^2); 
            dS=xdot;
        end
        S=Sv(:,9:10);
    end
 
datax=[path_profiles(init1:end5,5),velocity_profiles(init1:end5,5)];%
A5=[0,0];
[A15] = lsqcurvefit(@fit15,A5,time(init1:end5,1),datax,[0.5,0],[3,3]);

Cfit2=fit12(A12, time(init1:end5,1));
Cfit1=fit11(A11, time(init1:end5,1));
Cfit3=fit13(A13, time(init1:end5,1));
Cfit4=fit14(A14, time(init1:end5,1));
Cfit5=fit15(A15, time(init1:end5,1));

figure(1);
subplot(3,1,1);
plot(time(init1:end1,1), path_profiles(init1:end1,1),'b');
hold on
plot(time(init1:end2,1), path_profiles(init1:end2,2),'g');
hold on
plot(time(init1:end3,1), path_profiles(init1:end3,3),'r');
hold on
plot(time(init1:end4,1), path_profiles(init1:end4,4),'m');
hold on
plot(time(init1:end5,1), path_profiles(init1:end5,5),'c');
hold on
plot(time(init1:end5,1), Cfit1(:,1),'--b');
hold on
plot(time(init1:end5,1), Cfit2(:,1),'--g');
hold on
plot(time(init1:end5,1), Cfit3(:,1),'--r');
hold on
plot(time(init1:end5,1), Cfit4(:,1),'--m');
hold on
plot(time(init1:end5,1), Cfit5(:,1),'--c');
hold off
grid on
grid minor
set(gcf,'color','w');
set(gca,'XMinorTick','on','YMinorTick','on');
set(gca,'linewidth',1);
set(findall(gca, 'Type', 'Line'),'LineWidth',1.2);
xlim([time(init1,1),time(end5,1)]);
ylabel('distance [m]');
xlabel('time [s]');
title('Path profiles: IDM (dotted) vs Data (solid)');
 
%figure(2);
subplot(3,1,2);
plot(time(init1:end1,1), velocity_profiles(init1:end1,1),'b');
hold on
plot(time(init1:end2,1), velocity_profiles(init1:end2,2),'g');
hold on
plot(time(init1:end3,1), velocity_profiles(init1:end3,3),'r');
hold on
plot(time(init1:end4,1), velocity_profiles(init1:end4,4),'m');
hold on
plot(time(init1:end5,1), velocity_profiles(init1:end5,5),'c');
hold on
plot(time(init1:end5,1), Cfit1(:,2),'--b');
hold on
plot(time(init1:end5,1), Cfit2(:,2),'--g');
hold on
plot(time(init1:end5,1), Cfit3(:,2),'--r');
hold on
plot(time(init1:end5,1), Cfit4(:,2),'--m');
hold on
plot(time(init1:end5,1), Cfit5(:,2),'--c');
hold off
grid on
grid minor
set(gcf,'color','w');
set(gca,'XMinorTick','on','YMinorTick','on');
set(gca,'linewidth',1);
set(findall(gca, 'Type', 'Line'),'LineWidth',1.2);
xlim([time(init1,1),time(end5,1)]);
ylabel('velocity [m/s]');
xlabel('time [s]');
title('Velocity profiles: IDM (dotted) vs Data (solid)');
  
%figure(3);
subplot(3,1,3);
dydx1b = diff(Cfit1(:,2))./diff(time(init1:end5,1));
dydx2b = diff(Cfit2(:,2))./diff(time(init1:end5,1));
dydx3b = diff(Cfit3(:,2))./diff(time(init1:end5,1));
dydx4b = diff(Cfit4(:,2))./diff(time(init1:end5,1));
dydx5b = diff(Cfit5(:,2))./diff(time(init1:end5,1));

plot(time, acceleration_profiles(:,1),'b');
hold on
plot(time, acceleration_profiles(:,2),'g');
hold on
plot(time, acceleration_profiles(:,3),'r');
hold on
plot(time, acceleration_profiles(:,4),'m');
hold on
plot(time, acceleration_profiles(:,5),'c');
hold on

plot(time(init1:end5-1,1), dydx1b,'--b');
hold on
plot(time(init1:end5-1,1), dydx2b,'--g');
hold on
plot(time(init1:end5-1,1), dydx3b,'--r');
hold on
plot(time(init1:end5-1,1), dydx4b,'--m');
hold on
plot(time(init1:end5-1,1), dydx5b,'--c');
hold off
grid on
grid minor
set(gcf,'color','w');
set(gca,'XMinorTick','on','YMinorTick','on');
set(gca,'linewidth',1);
set(findall(gca, 'Type', 'Line'),'LineWidth',1.2);
xlim([time(init1,1),time(end5,1)]);
ylabel('acceleration [m/s^2]');
xlabel('time [s]');
title('Acceleration profiles: IDM (dotted) vs Data (solid)');

A=[A11,A12,A13,A14,A15];

Cfit=fit11(A11, time(init1:end1,1));
X1R2=determination(path_profiles(init1:end1,1),Cfit(:,1));
V1R2=determination(velocity_profiles(init1:end1,1),Cfit(:,2));
Cfit=fit12(A12, time(init1:end2,1));
X2R2=determination(path_profiles(init1:end2,2),Cfit(:,1));
V2R2=determination(velocity_profiles(init1:end2,2),Cfit(:,2));
Cfit=fit13(A13, time(init1:end3,1));
X3R2=determination(path_profiles(init1:end3,3),Cfit(:,1));
V3R2=determination(velocity_profiles(init1:end3,3),Cfit(:,2));
Cfit=fit14(A14, time(init1:end4,1));
X4R2=determination(path_profiles(init1:end4,4),Cfit(:,1));
V4R2=determination(velocity_profiles(init1:end4,4),Cfit(:,2));
Cfit=fit15(A15, time(init1:end5,1));
X5R2=determination(path_profiles(init1:end5,5),Cfit(:,1));
V5R2=determination(velocity_profiles(init1:end5,5),Cfit(:,2));

disp(' ');
disp('Estimated parameters of the IDM: ');
disp(['A1: ', num2str(A11)]);
disp(['A2: ', num2str(A12(1))]);
disp(['T2: ', num2str(A12(2))]);
disp(['A3: ', num2str(A13(1))]);
disp(['T3: ', num2str(A13(2))]);
disp(['A4: ', num2str(A14(1))]);
disp(['T4: ', num2str(A14(2))]);
disp(['A5: ', num2str(A15(1))]);
disp(['T5: ', num2str(A15(2))]);
disp(' ');
disp('Coefficients of determination (R^2) are the following: ');
disp(['X1 R^2: ', num2str(X1R2)]);
disp(['V1 R^2: ', num2str(V1R2)]);
disp(['X2 R^2: ', num2str(X2R2)]);
disp(['V2 R^2: ', num2str(V2R2)]);
disp(['X3 R^2: ', num2str(X3R2)]);
disp(['V3 R^2: ', num2str(V3R2)]);
disp(['X4 R^2: ', num2str(X4R2)]);
disp(['V4 R^2: ', num2str(V4R2)]);
disp(['X5 R^2: ', num2str(X5R2)]);
disp(['V5 R^2: ', num2str(V5R2)]);

    function [R2]=determination(measured,model)  
        average=mean(measured);
        ss_tot=sum((measured-average).^2);
        ss_res=sum((measured-model).^2);
        R2=abs(1-(ss_res/ss_tot));     
    end
end