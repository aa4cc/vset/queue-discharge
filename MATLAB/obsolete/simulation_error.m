function err = simulation_error(params, queue, model)
% runs a simulation of IDM and then computes error
% Input:
%   params = model parameters 
%   queues = cell array of individual cars in a queue
% Output:
%   err = array of mean square errors of individual cars

err = 0;
n = length(queue);
x_0 = zeros(2*n,1);
v_0 = zeros(2*n,1);
for vhcl = 1:n
    x_0(vhcl) = -queue{vhcl}.X(1);
    v_0(vhcl) = queue{vhcl}.V(1);
end
t0 = queue{1}.T(1);
t1 = queue{n}.T(end);

sim_q = simul(model,x_0, v_0,params,t1-t0);

for vhcl = 1:n
    sim_q{vhcl}.X = interp1(sim_q{vhcl}.T, sim_q{vhcl}.X,  queue{vhcl}.T-t0);   %interpolates simulated positions in original data's timestamps
    sim_q{vhcl}.V = interp1(sim_q{vhcl}.T, sim_q{vhcl}.V,  queue{vhcl}.T-t0);   %interpolates simulated velocities in original data's timestamps
    
    idx_X = ~logical(isnan(sim_q{vhcl}.X)+isinf(sim_q{vhcl}.X));      %finds indexes where interpolation yielded real numbers
    idx_V = ~logical(isnan(sim_q{vhcl}.V)+isinf(sim_q{vhcl}.V));      %finds indexes where interpolation yielded real numbers
    
    err_X = sim_q{vhcl}.X(idx_X)-queue{vhcl}.X(idx_X);          % difference between original and simulated position
    err_V = sim_q{vhcl}.V(idx_V)-queue{vhcl}.V(idx_V);          % difference between original and simulated velocity

    C1 = norm(err_X)/numel(err_X);  %mean square error of positions
    C2 = norm(err_V)/numel(err_V);  %mean square error of velocities
    C3 = (t1-t0-sim_q{vhcl}.T(end))/(t1-t0);      %simulation crash error
    
    err = err + C1 + C2 + abs(C1-C2) +1000*C3;
end

end