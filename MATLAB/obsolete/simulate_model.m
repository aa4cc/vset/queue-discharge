function [simulated_queue] = simulate_model(model,x_0, params, t1)
%runs a simulation from initial positions of a queue
%   uses time span from queue
n = length(x_0)/2;
simulated_queue = cell(1,n);

% set simulation time
tspan = [0,t1];


if      model == "IDM"
    options = odeset('RelTol',1e-6);
    [t,y] = ode45(@(t,x) idm(t, x, n, params), tspan, x_0, options);
    
elseif  model == "IIDM"
    options = odeset('RelTol',1e-6);
    [t,y] = ode45(@(t,x) iidm(t, x, n, params), tspan, x_0, options);
    
elseif  model == "LTI"
    [t,y] = LTI(tspan,x_0,n,params);
    
elseif  model == "IIDM_WD"
    options = ddeset('RelTol',1e-6);
    D = (1:n-1)' .* params(1);
    sol = dde23(@(t, x, xlag) iidm_with_delay(t, x, xlag, n, params), D, @(t) history_IIDM_WD(t, x_0), tspan, options);
    t = sol.x';
    y = sol.y';
else
    txt = "Wrong model name"
    return
end


% Assign values to similar data structure as original data for better
% access
for vhcl = 1:n
    simulated_queue{vhcl}.X = y(:,vhcl);
    simulated_queue{vhcl}.V = y(:,n+vhcl);
    simulated_queue{vhcl}.T = t;
end

end

