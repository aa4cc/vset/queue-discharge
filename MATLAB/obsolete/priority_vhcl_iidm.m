function dxdt = priority_vhcl_iidm(t, x, v_0, T, a, b, delta, l, s_0, s_1, last_vhcl, delay)
% Definition of Improved IDM for a priority vehicle
%parameteres:
%   input: 
%       t = time [s]
%       x = states (2*nx1) | x(1:n) = positions | x(n+1:2*n) = velocities
%       n = number of cars 
%       v_0 = desired velocities (nx1) [m/s]
%       T = desired time headways (nx1) [s]
%       a = maximum accelerations (nx1) [m/s^2]
%       b = maximum decelerations (nx1) [m/s^2]
%       delta = acceleration exponents (nx1) | usually set to 4
%       l = car lengths (nx1) [m]
%       s_0 = minimim linear bumper-to-bumper distance
%       s_1 = minimim nonlinear bumper-to-bumper distance
%       last_vhcl = last vehicle in queue
%   output:
%       dxdt = state derivatives (2*nx1) | dxdt(1:n) = velocities |
%       x(n+1:2*n) = accelerations
    
    if t <= delay
        l_x = -last_vhcl.X(1);
        l_v = 0;
    else
        l_x = -interp1(delay+last_vhcl.T-last_vhcl.T(1),last_vhcl.X,t);
        l_v = interp1(delay+last_vhcl.T-last_vhcl.T(1),last_vhcl.V,t);
    end
    
    dxdt = zeros(2,1); %preallocation 
    v = x(2);
    
    if v < 0         %cars cannot reverse
        dxdt(1) = 0;
        dxdt(2) = 0;
    else
        dxdt(1) = v; %velocity

        dv_i = l_v-v;         %relative velocity
        s_i = max(0.1,l_x-x(1)-l);   %bumper-to-bumper distance

        s_star = s_0 + s_1*sqrt(v/v_0) + T*v + v*dv_i/(2*sqrt(a*b));
        s_star = abs(s_star);
        
        z = s_star/s_i;
    
        if v <= v_0
            a_free = a*(1-(v/v_0)^delta);
            if z >= 1
                g = a*(1-z^2);
            else
                g = a_free*(1-z^((2*a)/a_free));
            end
        else
            a_free = -b*(1-(v_0/v)^(a*delta/b));
            if z >= 1
                g = a_free+a*(1-z^2);
            else
                g = a_free;
            end
        end
        
        dxdt(2) = g; %acceleration
    end
end