function g = g_func(s_i,v_i, dv_i, v_0, T, a, b, delta, s_0)
% Computes acceleration for a given vehicle
%   input:
%       s_i = bumper-to-bumper distance to the vehicle ahead
%       v_i = velocity of my vehicle
%       dv_i = relative velocity of the vehicle ahead
%       v_0 = desired speed       
%       T = minimum time headway
%       a = maximum acceleration
%       b = maximum deceleration
%       s_0 = minimim linear bumper-to-bumper distance
%   output:
%       g = acceleration of the vehicle

    %desired bumper to bumper distance
    s_star = s_0 + max([0,T*v_i + v_i*dv_i/(2*sqrt(a*b))]);
    
    %the original model for b2b distance
    %s_star = s_0 + s_1*sqrt(v_i/v_0) + T*v_i + v_i*dv_i/(2*sqrt(a*b));
    
    %acceleration
    g = a*(1-(v_i/v_0)^delta - (s_star/s_i)^2);
end

