function dydt = iidm_with_delay(t, y, ylag, n, params)
% Deffinition of Improved IDM
%parameteres:
%   input:
%       t = time [s]
%       y = states (2*nx1) | y(1:n) = positions
%                            y(n+1:2*n) = velocities
%       ylag = delayed states  - ylag(state,delay_index)
%       n = number of cars
%   params:
%       D = information lag (1x1) or (nx1) [s]
%       v_0 = desired velocities (1x1) or (nx1) [m/s]
%       T = desired time headways (1x1) or (nx1) [s]
%       a = maximum accelerations (1x1) or (nx1) [m/s^2]
%       b = maximum decelerations (1x1) or (nx1) [m/s^2]
%       l = car lengths (1x1) or (nx1) [m]
%       s_0 = minimim linear bumper-to-bumper distance (1x1) or (nx1) [m]
%   output:
%       dydt = state derivatives (2*nx1) | dydt(1:n) = velocities |
%                                          dydt(n+1:2*n) = accelerations

dydt = zeros(2*n,1); %preallocation
x_lag = zeros(n,1);
v_lag = zeros(n,1);

common_params = (size(params,1) == 1);

x = y(1:n);
v = y(n+1:2*n);

for i=1:n
    if i==1
        x_lag(i) = y(i);
        v_lag(i) = y(n+i);
    else
        x_lag(i) = ylag(i,i-1);
        v_lag(i) = ylag(n+i,i-1);
    end
end


for i=1:n
    
    reversing = (v(i) < 0);
    
    v(i) = max(0, v(i));
    
    dydt(i) = v(i);
    
    if i==1         % first car follows a fictitious vehicle
        dv_i = 0;   % fake car has zero relative velocity to the first car
        s_i = 100;  % fake car is 100 m ahead
        
    else                            %other cars follow the car ahead
        dv_i = v_lag(i-1)-v_lag(i);         %relative velocity
           %bumper-to-bumper distance
        if common_params
            s_i = max(0.0001,x_lag(i-1)-x_lag(i)-params(6));   %bumper-to-bumper distance (max because we don't have actual lengths of cars)
        else
            s_i = max([0.0001,x_lag(i-1)-x_lag(i)-params(i-1,6)]);
        end
    end
    if common_params
        dydt(n+i) = i_g_func(s_i, v_lag(i), dv_i, params(2), params(3), params(4), params(5), 4, params(7)); %acceleration
    else
        dydt(n+i) = i_g_func(s_i, v_lag(i), dv_i, params(i,2), params(i,3), params(i,4), params(i,5), 4, params(i,7)); %acceleration
    end
    
    
    if reversing
        dydt(n+i) = max(0, dydt(n+i));
    end
end
end
