%%
% Script to identify model parameters. Fits the model on real data
% TODO try to supply jacobians
format long;

addpath('model_functions');

MODEL = "LTI"; %choose model - IDM, IIDM, IIDM_WD or LTI
INTERSECTION = 2; %choose intersection
LANE = 17;        %choose lane
QUEUE = 1;        %choode queue (you can find how many there are by reading queues_len)
STOP_TIME = 5;    %how long a car needs to go very slowly to be considered a part of a queue

%% Generate data
addpath('data_functions');
A = read_data(INTERSECTION);            %reads raw data
lane = get_lane(A, INTERSECTION, LANE); %extracts only cars from selected lane
lane = filter_lane(lane, STOP_TIME);    %filters cars that are part of a queue
queues = split_queues(lane);            %splits queues so they can be analyzed separately
queue = queues{QUEUE};                  %extracts selected queue
queue_len = length(queue);              %holds the number of vehicles in queue

%% Paramters for all models
if MODEL == "IIDM" || MODEL == "IDM"
    options = optimoptions('lsqcurvefit', 'Algorithm', 'levenberg-marquardt','ScaleProblem', 'jacobian', 'InitDamping', 1e-10);
    %          [v_0,    T,      a,      b,      l,      s_0,    delta]
    param_0 =  [15,     1.2,    2,      2,      4,      2,      4];  %initial guess for the parameters [v_0,T,a,b,l,s_0, delta]
    param_lb = [10,     0.5,    1,      1,      3,      0.5,    1];  %lower bound on parameters
    param_ub = [20,     10,     5,      5,      7,      10      20];  %upper bound on parameters
elseif MODEL == "LTI"
    options = optimoptions('lsqcurvefit', 'Algorithm', 'levenberg-marquardt', 'InitDamping', 1e-6);
    %          [k,    v_0,      T,      a]
    param_0 =  [15,       1,      3];  %initial guess for the parameters [v_0,T,a]
    param_lb = [14,       0.1,    0.1];  %lower bound on parameters
    param_ub = [16,       3,      10];  %upper bound on parameters
elseif MODEL == "IIDM_WD"
    options = optimoptions('lsqcurvefit', 'Algorithm', 'levenberg-marquardt','ScaleProblem', 'jacobian', 'InitDamping', 1e-10);
    iidm_params = load("./model_parameters/IIDM_params_average.mat").average_params;
    %          [v_0,    T,      a,      b,      l,      s_0,    delta,  D]
    param_0 =  [iidm_params,                                            0.1];  %initial guess for the parameters [v_0,T,a,b,l,s_0, D]
    param_lb = [10,     0.5,    2,      1,      3,      0.5,    1,      0];  %lower bound on parameters
    param_ub = [20,     10,     5,      5,      7,      10,     20,     0.5];    %upper bound on parameters           
end


%% Fitting
options = optimoptions(options,'Display', 'none');
% To get INFO output use this:
options = optimoptions(options,'Display', 'iter');
% To use supplied Jacobian use this:
%options = optimoptions(options, 'SpecifyObjectiveGradient',true);
% If you find how to compute jacobians check with this:
%options = optimoptions(options, 'CheckGradients', true, 'FiniteDifferenceType', 'central', 'FiniteDifferenceStepSize', 1e-10);

x0 = zeros(queue_len,1);
v0 = zeros(queue_len,1);

prev_params = [];
for vhcl = 1:queue_len
    x0(vhcl) = queue{vhcl}.X(1);
    v0(vhcl) = queue{vhcl}.V(1);
    T = queue{vhcl}.T-queue{vhcl}.T(1);
    X = queue{vhcl}.X';
    V = queue{vhcl}.V';
    Y = [X;V];
    [best_params, resnorm] = lsqcurvefit(@(params, t) run_params(params, t, prev_params, MODEL, x0(1:vhcl), v0(1:vhcl)), param_0, T, Y, param_lb, param_ub, options);
    prev_params = [prev_params;best_params];
end

separated_params = prev_params;
average_params = mean(prev_params,1);



sprintf("LSQ error: %f", resnorm)

save("./model_parameters/"+MODEL+"_params_separated.mat",'separated_params');
save("./model_parameters/"+MODEL+"_params_average.mat",'average_params');

%% Check that visualize_model uses the same madel

%visualize_model;
