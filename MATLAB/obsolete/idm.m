function dxdt = idm(t, x, n, params)
% Deffinition of IDM
%parameteres:
%   input:
%       t = time [s]
%       x = states (2*nx1) | x(1:n) = positions | x(n+1:2*n) = velocities
%       n = number of cars
%   params: [v_0, T, a, b, delta, l, s_0] (1x6) or (nx6)
%       v_0 = desired velocities (1x1) or (nx1) [m/s]
%       T = desired time headways (1x1) or (nx1) [s]
%       a = maximum accelerations (1x1) or (nx1) [m/s^2]
%       b = maximum decelerations (1x1) or (nx1) [m/s^2]
%       l = car lengths (nx1) [m]
%       s_0 = minimim linear bumper-to-bumper distance
%   output:
%       dxdt = state derivatives (2*nx1) | dxdt(1:n) = velocities |
%       x(n+1:2*n) = accelerations

dxdt = zeros(2*n,1); %preallocation
v = x((n+1):(2*n));

common_params = (size(params,1) == 1);

for i=1:n
    
    dxdt(i) = v(i); %velocity
    
    if i==1         % first car follows a fictitious vehicle
        dv_i = 0;   % fake car has zero relative velocity to the first car
        s_i = 100;  % fake car is 100 m ahead
        
    else                            %other cars follow the car ahead
        dv_i = v(i-1)-v(i);         %relative velocity
        if common_params
            s_i = max(0.0001,x(i-1)-x(i)-params(5));   %bumper-to-bumper distance (max because we don't have actual lengths of cars)
        else
            s_i = max(0.0001,x(i-1)-x(i)-params(i-1,5));
        end
    end
    if common_params
        dxdt(n+i) = g_func(s_i, v(i), dv_i, params(1), params(2), params(3), params(4), 4, params(6)); %acceleration
    else
        dxdt(n+i) = g_func(s_i, v(i), dv_i, params(i,1), params(i,2), params(i,3), params(i,4), 4, params(i,6)); %acceleration
    end
    
end
end


