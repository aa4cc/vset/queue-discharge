function [p_cost, varargout] = priority_vhcl_cost(delay, d_init, d_des, v_init, last_car, params, weight)
% computes cost for green signal switching when priority vehicle is "dist"
% away
%

weight = min([1, max([1e-2, 1-weight])]);

t0 = 0;
t1 = delay + last_car.T(end)-last_car.T(1);
tspan = [t0, t1];

v_0 = v_init;   %let's assume the initial velocity is desired velocity;
T = 3;
a = v_0;
b = v_0;
delta = 4;
l = 4;
s_0 = 3;
s_1 = 0;

x_0 = [-d_init; v_init];
%options = odeset('RelTol',1e-6, 'OutputFcn', @odeplot);
options = odeset('RelTol',1e-6);
[t,y] = ode45(@(t,x) priority_vhcl_iidm(t, x, v_0, T, a, b, delta, l, s_0, s_1, last_car, delay), tspan, x_0, options);

X = y(:,1);
V = y(:,2);

crash = min([find(isnan(X),1), find(isinf(X),1), find(isnan(V),1), find(isinf(V),1), find(isempty(X),1), find(isempty(V),1)]);
C1 = 0;
C2 = 0;
C3 = 0;

max_delay = d_init/v_init;

if isempty(crash)
    des_X = -d_init + v_init*t;
    C1 = norm(X(X<d_des) - des_X(X<d_des))/norm(des_X(X<d_des));
    C2 = max(0,(max_delay - delay)/max_delay)^(1/weight);
else
    txt = "priority vehicle crashed - change model parameters"
    C3 = (t1 - t(crash))/t1;
end

p_cost = max(C1,C2)+C3;

if nargout == 4
    varargout{1} = C1;
    varargout{2} = C2;
    varargout{3} = C3;
end

end

