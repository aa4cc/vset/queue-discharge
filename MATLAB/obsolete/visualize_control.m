%% Script to visualize the optimal trajectory of a priority vehicle as well as the optimal time of green signal
% The trajectory of the priority vehicle minimizes change in velocity and
% time to cross intersection
format long;

%%
%QUEUE PARAMETERS
MODEL = "IIDM";    % choose model for waiting vehicles - IDM, IIDM or LTI
INTERSECTION = 2; % choose intersection 
LANE = 17;        % choose lane
QUEUE = 1;
STOP_TIME = 5;    % how long a car needs to go very slowly to be considered a part of a queue

%%
%PRIORITY VEHICLE PARAMETERS
INIT_V = 70/3.6;  % initial velocity of priority vehicle (m/s)
INIT_D = 1000;    % initial distance of priority vehicle from intersection
DES_D = 20;       % distance after the traffic light we track
PRIORITY_WEIGHT = 0.7;  % [0-1] - weight determining how smoothly is a priority vehicle required to pass the intersection 

%%
%OUTPUT OPTIONS
close all;
out_dir = "../pictures/visualize_control/";
f = figure('Position', [1,1,800,400]);
out_format1 = 'epsc';
out_format2 = 'png';
fontsize = 15;
linewidth = 2;

% legend entries
% cz
rv_cz = "re\'{a}ln\'{e} vozidlo";
sv_cz = "simulovan\'{e} vozidlo";
pv_cz = "preemp{\v c}n\'{i} vozidlo"; 
% en
rv_en = "real vehicle";
sv_en = "simulated vehicle";
pv_en = "preemption vehicle";

%%
% Loads data, preprocessing
addpath('data_functions');
A = read_data(INTERSECTION);            %reads raw data
lane = get_lane(A, INTERSECTION, LANE); %extracts only cars from selected lane
lane = filter_lane(lane, STOP_TIME);    %filters cars that are part of a queue
queues = split_queues(lane);            %splits queues so they can be analyzed separately
queues_len = length(queues);            %holds the number of queues extracted

%%
% simulates model for the queue
addpath('model_functions');
best_params = load("./model_parameters/"+MODEL+"_params.mat").best_params;
queue = queues{QUEUE};
n = length(queue);
x_0 = zeros(n,1);
v_0 = zeros(n,1);
for vhcl = 1:n
    x_0(vhcl) = queue{vhcl}.X(1);
    v_0(vhcl) = queue{vhcl}.V(1);
end
dt = queue{1}.T(2)-queue{1}.T(1);
T = 0:dt:(INIT_D+DES_D+10)/INIT_V;

sim_q = simul(MODEL, x_0, v_0, best_params, T);

%%
% signal switch optimization
addpath('preemption_optimization');
last_car = sim_q{end};
p_v_params = load("./model_parameters/IIDM_params.mat").best_params;
delay_init = 0;
delay_lb = 0;
delay_ub = (INIT_D+DES_D)/INIT_V;
options = optimoptions('fmincon', 'Display', 'iter', 'OptimalityTolerance', 1e-10);
best_delay = fmincon(@(delay) emergency_cost(delay, -INIT_D, INIT_V, DES_D, last_car, p_v_params, "IIDM", PRIORITY_WEIGHT), delay_init, [], [], [], [], delay_lb, delay_ub, [] ,options);

%%
% priority vehicle simulation with optimal signal swicth delay

[c, c1, c2, y] = emergency_cost(best_delay, -INIT_D, INIT_V, DES_D, last_car, p_v_params, "IIDM", PRIORITY_WEIGHT);

priority_vhcl.X = y(1,:);
priority_vhcl.V = y(2,:);
priority_vhcl.T = y(3,:)-best_delay;

%%
% position
maxpos = 0;
minpos = priority_vhcl.X(1);
queue_len = length(sim_q);
clf;
hold on;
plot(priority_vhcl.T, priority_vhcl.X, 'Color', '#0072BD', 'Linewidth', linewidth);
for vhcl=1:queue_len
    m_p = max(queues{QUEUE}{vhcl}.X);
    if m_p > maxpos
        maxpos = m_p;
    end
    plot(queues{QUEUE}{vhcl}.T-queues{QUEUE}{vhcl}.T(1), queues{QUEUE}{vhcl}.X, '--', 'Color', '#EDB120', 'LineWidth', linewidth);
    plot(sim_q{vhcl}.T, sim_q{vhcl}.X, '-', 'Color', '#D95319', 'LineWidth', linewidth);
end
hold off;
grid;
ylim([minpos, maxpos*(11/10)]);
xlim([min(priority_vhcl.T), max(priority_vhcl.T(priority_vhcl.X < maxpos*(11/10)))]);
legend(pv_en, rv_en, sv_en, 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'southeast');
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$x [m]$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_POS_EN", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_POS_EN", out_format2);
legend(pv_cz, rv_cz, sv_cz, 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'southeast');
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_POS_CZ", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_POS_CZ", out_format2);

%%
% Velocity
clf;
hold on;
plot(priority_vhcl.T, priority_vhcl.V, 'Color', '#0072BD', 'LineWidth', linewidth);
for vhcl=1:queue_len
    plot(queues{QUEUE}{vhcl}.T-queues{QUEUE}{vhcl}.T(1), queues{QUEUE}{vhcl}.V, '--', 'Color', '#EDB120', 'LineWidth', linewidth);
    plot(sim_q{vhcl}.T, sim_q{vhcl}.V, '-', 'Color', '#D95319', 'LineWidth', linewidth);
end
hold off;
grid;
xlim([min(priority_vhcl.T), max(priority_vhcl.T)]);
legend(pv_en, rv_en, sv_en, 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'southeast');
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$v \left[ \frac{m}{s}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_VEL_EN", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_VEL_EN", out_format2);
legend(pv_cz, rv_cz, sv_cz, 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'southeast');
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_VEL_CZ", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_VEL_CZ", out_format2);

%%
% cost function


N = 100;
priorities = 0:0.1:1;
c = zeros(1,N);
c1 = zeros(1,N);
c2 = zeros(1,N);
c3 = zeros(1,N);
d = linspace(delay_lb, delay_ub, N);
for n = 1:N
    [c(n), c1(n), c2(n)] = emergency_cost(d(n), -INIT_D, INIT_V, DES_D, last_car, p_v_params, "IIDM", PRIORITY_WEIGHT);
end

%%
clf;
hold on;
plot(d, c,'Linewidth', linewidth);
plot(d, [c1;c2],'--','Linewidth', linewidth);
hold off;
xlim([min(d), max(d)]);
grid;
legend("cost function", "deceleration cost", "early trigger cost", 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'northwest');
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$J$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_COST_EN", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_COST_EN", out_format2);
legend("kriteri\'{a}ln\'{i} funkce", "cena za br{\v z}d{\v e}n\'{i}", "cena za p{\v r}ed{\v c}asn\'{e} spu{\v s}t{\v e}n\'{i}", 'Interpreter', 'latex', 'FontSize', fontsize, 'Location', 'northwest');
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_COST_CZ", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_COST_CZ", out_format2);
