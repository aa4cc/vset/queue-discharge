function g = i_g_func(s_i,v_i, dv_i, v_0, T, a, b, delta, s_0)
% Computes acceleration for a given vehicle
%   input:
%       s_i = bumper-to-bumper distance to the vehicle ahead
%       v_i = velocity of my vehicle
%       dv_i = relative velocity of the vehicle ahead
%       v_0 = desired speed       
%       T = minimum time headway
%       a = maximum acceleration
%       b = maximum deceleration
%       s_0 = minimim linear bumper-to-bumper distance
%   output:
%       g = acceleration of the vehicle
    
    %desired bumper to bumper distance
    s_star = s_0 + max([0, T*v_i+v_i*dv_i/(2*sqrt(a*b))]);
    
    
    z = s_star/s_i;
    
    if v_i <= v_0
        a_free = a*(1-(v_i/v_0)^delta);
        if z >= 1
            g = a*(1-z^2); 
        else
            g = a_free*(1-z^((2*a)/a_free));
        end
    else
        a_free = -b*(1-(v_0/v_i)^(a*delta/b));
        if z >= 1
            g = a_free+a*(1-z^2); 
        else
            g = a_free;
        end
    end
    if abs(imag(g)) > 0
        g
    end
end

