function [p_cost, varargout] = emergency_cost(delay, x_init, v_init, d_des, last_car, parameters, model, weight)
%EMERGENCY_COST
% computes cost for green signal switching when priority vehicle is "dist"
% away

weight = min([1, max([1e-2, 1-weight])]);

t_end = delay + last_car.T(end)-last_car.T(1);
x0 = [x_init; v_init];

params = parameters;

dt = last_car.T(2) - last_car.T(1);

t_pre = 0:dt:delay-dt;
t_aft = last_car.T-last_car.T(1)+delay;

vl_pre = zeros(1, numel(t_pre));
xl_pre = last_car.X(1).*ones(1, numel(t_pre));

xl = [xl_pre, last_car.X;
      vl_pre, last_car.V];
tl = [t_pre, t_aft];

xl = xl(:,tl <= t_end);
tl = tl(tl <= t_end);

tspan = [tl(1), tl(end)];


options = odeset('RelTol',1e-6);
if model == "IDM"
    params(1) = v_init;
    s = ode45(@(t,x) IDM(t, x, tl, xl, params), tspan, x0, options);
elseif model == "IIDM"
    params(1) = v_init;
    s = ode45(@(t,x) IIDM(t, x, tl, xl, params), tspan, x0, options);
elseif model == "IIDM_WD"
    params(1) = v_init;
    s = ode45(@(t,x) IIDM_WD(t, x, tl, xl, params), tspan, x0, options);
elseif model == "LTI"   %CURRENTLY IT MAKES NO SENSE TO USE THIS MODEL HERE
    params(1) = vhcl;
    params(2) = v_init;
    s = ode45(@(t,x) LTI(t, x, tl, xl, params), tspan, [x0(1);0], options);
else
    sprintf("Add more models.");
end

y = deval(s,tl);

X = y(1,:);
V = y(2,:);


max_delay = abs(x_init/v_init);
des_X = x_init + v_init*tl;

abs_dA = abs(diff(V));
abs_A = abs_dA(X(1:end-1)<=d_des)*tl(X(1:end-1)<=d_des)';
max_A = v_init*t_end;

C1 = abs_A/max_A;
C2 = max(0,(max_delay - delay)/max_delay)^(1/weight);
p_cost = max(C1,C2);

if nargout >= 3
    varargout{1} = C1;
    varargout{2} = C2;
end

if nargout == 4
    varargout{3} = [y;tl];
end

end

