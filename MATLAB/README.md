## MATLAB SCRIPTS

### [visualize_lane.m](./visualize_lane.m)

![output of visualize_lane.m](../pictures/visualize_lane/I_2_L_17_POS.png "output of visualize_lane.m for INTERSECTION 2, LANE 17")

Shows all data from a single lane.

1. in [raw_data](./raw_data) choose an intersection and a lane from CR\<X\>\_WITH\_GATES_SNAPSHOT.png, where \<X\> is a number of the intersection.
2. edit the values for INTERSECTION and LANE at the begining of the file.
3. run the script

### [visualize_model.m](./visualize_model.m)

![position from visualize_model.m](../pictures/visualize_model/I_2_L_17_Q_1_IIDM_SEP_POS_EN.png "position output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

![velocity from visualize_model.m](../pictures/visualize_model/I_2_L_17_Q_1_IIDM_SEP_VEL_EN.png "velocity output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

Runs a selected estimation model on initial positions from queues of a given lane.

1. run visualize_lane.m to make sure your chosen lane includes interesting data
2. make sure INTERSECTION and LANE are correctly selected
3. choose from supported models using the variable MODEL
4. run the script

### [evaluate_models.m](./evaluate_models.m)

![evaluation from evaluat_models.m](../pictures/evaluate_models/SEP_LAST_VEH_V_RMSE.png "ovarall accuracy of fitting various models")

Fits car-following models to real data. Then it evaluates this fit and produces box charts.

1. If __REFIT == true__ then the model parameters are recomputed and saved to [opt_params.mat](./model_parameters/opt_params.mat)
2. __MODE__ sets whether parameters are fitted sequentially for each vehicle or for the whole queue at the same time
3. run the script 
