%% Script to compare the real-world data with the model estimate
%
close all;
format long;
addpath('model_functions');


MODELS = ["IDM", "IIDM", "LTI", "IIDM_WD"];
MODEL = "LTI"; %choose model - IDM, IIDM, IIDM_WD or LTI
MODE = "AVG"; %choose mode - SEP, AVG (separate or average parameters for individual vehicles)
INTERSECTION = 2; %choose intersection 
LANE = 17;        %choose lane
STOP_TIME = 5;    %how long a car needs to go very slowly to be considered a part of a queue
QUEUE = 1;

% legend entries
% cz
rv_cz = "re\'{a}ln\'{e} vozidlo";
sv_cz = "simulovan\'{e} vozidlo";
% en
rv_en = "real vehicle";
sv_en = "simulated vehicle";

out_dir = "../pictures/visualize_model/";
f = figure('Position', [1,1,600,600]);
out_format1 = 'epsc';
out_format2 = 'png';
fontsize = 20;
linewidth = 2;

%%
% Loads data, preprocessing
addpath('data_functions');
A = read_data(INTERSECTION);            %reads raw data
lane = get_lane(A, INTERSECTION, LANE); %extracts only cars from selected lane
lane = filter_lane(lane, STOP_TIME);    %filters cars that are part of a queue
queues = split_queues(lane);            %splits queues so they can be analyzed separately
queue = queues{QUEUE};                  %holds requested queue
queue_len = length(queue);              %number of vehicles in queue


%%
% simulates model for all queues in lane


model_index = find(MODELS==MODEL);
if MODE == "SEP"
    opt_params = load("./model_parameters/opt_params.mat").OPT_PARAMS{INTERSECTION}{LANE}{QUEUE}{model_index};
elseif MODE == "AVG"
    temp = load("./model_parameters/opt_params.mat").OPT_PARAMS{INTERSECTION}{LANE};
    opt_params = [];
    for q=1:length(temp)
        opt_params = [opt_params; temp{q}{model_index}];
    end
    opt_params = mean(opt_params,1);
    if MODEL ~= "LTI"
        temp = zeros(queue_len, numel(opt_params));

        temp(1,:) = opt_params;
        for vhcl=2:queue_len
            temp(vhcl,:) = opt_params;
            temp(vhcl,6) = min(opt_params(6), queue{vhcl-1}.X(1) - opt_params(5) - queue{vhcl}.X(1));
        end
        opt_params = temp;
    end
    
else
    sprintf("Nonexistant mode selected")
end
if isempty(opt_params)
    sprintf("Optimal params not found backup params will be loaded");
    opt_params = load("./model_parameters/"+MODEL+"_params_average.mat").average_params;
end

x_0 = zeros(queue_len,1);
v_0 = zeros(queue_len,1);
for vhcl=1:queue_len
    x_0(vhcl) = queue{vhcl}.X(1);
    v_0(vhcl) = queue{vhcl}.V(1);
end
sim_q = simul(MODEL, x_0, v_0, opt_params, queue{end}.T-queue{end}.T(1));
%% Visualize positions

min_X = 0;
max_X = 0;
min_V = 0;
max_V = 0;

queue = queues{QUEUE};
queue_len = length(queue);
clf;
hold on;
for vhcl=1:queue_len
    if max(queue{vhcl}.X) > max_X
        max_X = max(queue{vhcl}.X);
    end
    if min(queue{vhcl}.X) < min_X
        min_X = min(queue{vhcl}.X);
    end


    p1 = plot(queue{vhcl}.T-queue{1}.T(1), queue{vhcl}.X, '--', 'LineWidth', linewidth);
    p2 = plot(sim_q{vhcl}.T(sim_q{vhcl}.T <= queue{vhcl}.T(end)-queue{1}.T(1)), sim_q{vhcl}.X(sim_q{vhcl}.T <= queue{vhcl}.T(end)-queue{1}.T(1)), 'LineWidth', linewidth);
    p2.Color = p1.Color;
end
hold off;
grid;
ylim([min_X,max_X])
legend(rv_en, sv_en, 'Interpreter', 'latex', 'FontSize', fontsize, 'location', 'southeast');
xlabel('$t \, \left[\mathrm{s}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$x \, \left[\mathrm{m} \right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
set(gca, 'FontSize', fontsize, 'TickLabelInterpreter', 'latex');

InSet = get(gca, 'TightInset');
set(gca, 'Position', [InSet(1:2), 1-InSet(1)-InSet(3), 1-InSet(2)-InSet(4)]);

saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_POS_EN", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_POS_EN", out_format2);
legend(rv_cz, sv_cz, 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_POS_CZ", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_POS_CZ", out_format2);

%% Visualize velocities
clf;
hold on;
for vhcl=1:queue_len
    if max(queue{vhcl}.V) > max_V
        max_V = max(queue{vhcl}.V);
    end
    if min(queue{vhcl}.V) < min_V
        min_V = min(queue{vhcl}.V);
    end
    p3 = plot(queue{vhcl}.T-queue{1}.T(1), queue{vhcl}.V, '--', 'LineWidth', linewidth);
    p4 = plot(sim_q{vhcl}.T(sim_q{vhcl}.T <= queue{vhcl}.T(end)-queue{1}.T(1)), sim_q{vhcl}.V(sim_q{vhcl}.T <= queue{vhcl}.T(end)-queue{1}.T(1)), 'LineWidth', linewidth);
    p4.Color = p3.Color;
end
hold off;
grid;
legend(rv_en, sv_en, 'Interpreter', 'latex', 'FontSize', fontsize, 'location', 'southeast');
xlabel('$t \, \left[\mathrm{s}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$v \, \left[\mathrm{ms^{-1}}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
set(gca, 'FontSize', fontsize, 'TickLabelInterpreter', 'latex');
InSet = get(gca, 'TightInset');
set(gca, 'Position', [InSet(1:2), 1-InSet(1)-InSet(3), 1-InSet(2)-InSet(4)]);

saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_VEL_EN", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_VEL_EN", out_format2);
legend(rv_cz, sv_cz, 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_VEL_CZ", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_Q_" + num2str(QUEUE) +"_"+ MODEL + "_"+ MODE +"_VEL_CZ", out_format2);




