function lane = get_lane(A, intersection, from)
% Extracts cars from one lane and transforms their data
%   Input:
%       A = unfiltered data from an intrsection
%       intersection = number of intersection
%       from = gate (lane) to investigate
%
    [O, X, Y] = get_coords(intersection, from);         %retrievs coordinates of the gate
    datalen = idivide(size(A(:,9:end),2), int16(6));    %size of data
    lane = {};
    for vhcl=1:size(A,1)
        % reads entry gate
        ingate = textscan(cell2mat(A(vhcl,3)), ['%s', '%d']);
        ingate = cell2mat(ingate(2));
        
        if logical(~sum(ingate == from))
            continue
        end
    
        %reads x and y coords of a car
        x = str2double(A(vhcl,9+(0:6:6*(datalen-1))));
        x = x(~isnan(x))-O(1);
        y = str2double(A(vhcl,10+(0:6:6*(datalen-1))));
        y = y(~isnan(y))-O(2);
        %reads t
        t = str2double(A(vhcl,14+(0:6:6*(datalen-1))));
        t = t(~isnan(t));
    
        %transforms x and y to coords of intersection
        xt = [x;y]'*X;
        yt = [x;y]'*Y;
    
        %computes velocity and applies Savitzky–Golay filter
        v_ms = vecnorm([diff(xt)'; diff(yt)'])./ diff(t);
        V_smooth = sgolayfilt(v_ms,3,111);
        V_smooth(V_smooth<0)=0;
    
        %computes acceleration from velocities and applies Savitzky–Golay filter
        dt = diff(t);
        acc = diff(V_smooth)./(dt(2:end));
        A_smooth = sgolayfilt(acc,3,111);
    
        %creates a struct to write to
        lane{vhcl}.X = xt';
        lane{vhcl}.Y = yt';
        lane{vhcl}.V = V_smooth;
        lane{vhcl}.T = t;
        lane{vhcl}.A = A_smooth;
    end
    % removes empty cells
    lane = lane(~cellfun('isempty',lane));
end

