function [O, X, Y] = get_coords(intersection, from)
% Returns coordinates of gates for each intersection
%   Input:
%       intersection = number of an intersection
%       from = gate (lane) number
%   Output:
%       O = origin (left end) of the gate in world coordinates
%       X = X-axis direction in world coordinates (direction from which
%       cars approach)
%       Y = Y-axis direction in world coordinates (direction to the other
%       end of the gate)

    %ingate numbers in intersection for each intersection
    INGATES{1} = 6:13;
    INGATES{2} = 7:21;
    

    % real-world coordinates of left ends of ingates from the perspective of
    % cars for each intersection in order of INGATES
    ORIG{1} = [545038.10, 545041.88, 545050.19, 545040.00, 545036.28, 545032.59, 545021.82, 545021.66, 545022.01;
               5263721.96, 5263722.12, 5263731.88, 5263752.47, 5263752.26, 5263751.97, 5263734.22, 5263730.81, 5263727.82];
           
    ORIG{2} = [540713.13, 540716.49, 540719.61, 540722.56, 540725.57, 540761.54, 540761.30, 540741.58, 540738.75, 540735.59, 540732.36, 540729.08, 540725.63, 540701.31, 540701.62;
               6902975.41, 6902976.03, 6902976.51, 6902977.00, 6902977.44, 6902995.62, 6902998.34, 6903011.77, 6903011.60, 6903011.04, 6903010.75, 6903010.45, 6903012.22, 6903001.26, 6902998.06];                                                                                     

    % real-world coordinates of right ends of ingates from the perspective of
    % cars for each intersection in order of INGATES
    Y_dir{1} = [545041.88, 545045.58, 545053.25, 545036.28, 545032.59, 545029.48, 545021.66, 545022.01;
                5263722.12, 5263722.31,5263729.56, 5263752.26, 5263751.97, 5263751.59, 5263730.81, 5263727.82];
            
    Y_dir{2} = [540716.49, 540719.61, 540722.56, 540725.57, 540728.52, 540761.30, 540761.05, 540738.75, 540735.59, 540732.36, 540729.08, 540725.93, 540722.68, 540701.62, 540702.06;
                6902976.03, 6902976.51, 6902977.00, 6902977.44, 6902978.05, 6902998.34, 6903001.08, 6903011.60, 6903011.04, 6903010.75, 6903010.45, 6903010.10, 6903011.87, 6902998.06, 6902994.17];
    
            
    % finds the coordinates we require
    O = ORIG{intersection}(:, INGATES{intersection}==from);
    
    if isempty(O)
        O = [0;0];
        X = O;
        Y = O;
    else
    
        Y = Y_dir{intersection}(:,INGATES{intersection}==from) - ORIG{intersection}(:,INGATES{intersection}==from);

        Y = Y./norm(Y);     % normalize y-vector
        X = [-Y(2); Y(1)];  % compute x-vector   
    end
end

