function queues = split_queues(lane)
%Splits queues in a lane
%   Finds a first car in a queue and then adds to this queue all cars
%   waiting behind. Next car after that is a new first car and so on

lane_len = length(lane);
queues = {};
q_num = 1;

start = 1;
while start <= lane_len
    T0 = find_start(lane{start});
    for next = start:lane_len
        if logical(sum(lane{next}.T == T0))
            last = next;
        end
    end
    queue = {};
    for curr = start:last
        k = find(lane{curr}.T == T0,1);
        v.X = lane{curr}.X(k+2:end);
        v.Y = lane{curr}.Y(k+2:end);
        v.V = lane{curr}.V(k+1:end);
        v.A = lane{curr}.A(k:end);
        v.T = lane{curr}.T(k+2:end);
        queue{curr+1-start} = v;
    end
    queues{q_num} = queue;
    q_num = q_num + 1;
    start = last+1;
    
end


end

