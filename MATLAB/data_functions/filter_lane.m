function filt_lane = filter_lane(lane, stop_len)
%Filters out cars that passed the intersection without stopping
%   Input:
%       lane = cell array of individual vehicles
%       stop_len = how long the car has to move slowly not to be excluded
%       [s]
%   Output:
%       filt_lane  = lane without cars that did not stop
lane_len = length(lane);
framerate = lane{1}.T(2)-lane{1}.T(1);

filt_lane = {};

for vhcl = 1:lane_len
    X = lane{vhcl}.X;
    nbins = round(max(X) - min(X));
    [N,E] = histcounts(X, nbins);
    if max(N) >= stop_len/framerate
        filt_lane{vhcl} = lane{vhcl};
    end
end

% removes empty cells
filt_lane = filt_lane(~cellfun('isempty',filt_lane));
end

