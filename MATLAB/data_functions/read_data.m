function A = read_data(intersection)
%Reads all vehicle data from a .csv file
%   Input:
%       intersection = number of an interscetion
%   Output:
%       A = All vehicles sorted by time of crossing the gates
    p = "./raw_data";
    addpath(genpath(p));
    file = p+"/CR"+int2str(intersection)+"_ALL.csv";
    opts = detectImportOptions(file);
    opts.Delimiter = ';';
    A =  readmatrix(file, opts);
    
    % sorts data based on entry time
    EntryTime = str2double(A(:,4));
    [B,I] = sort(EntryTime);
    A = A(I,:);
end

