function T0 = find_start(vhcl)
%Finds time at which the car starts to accelerate
%
threshold = 1; %acceleration that needs to be surpassed
j = find(vhcl.A <= 0, 1); % car needs to first stop
k = find(vhcl.A(j+1:end) > threshold, 1); %then the car needs to accelerate 
m = find(vhcl.A(k:-1:1) <= 0, 1); % then we find the last time it was stationary
T0 = vhcl.T(j+k-m+2); % time at which the car started accelerating
end

