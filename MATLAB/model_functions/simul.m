function [varargout] = simul(model,x_0, v_0, params, T)
%runs a simulation from initial positions of a queue
%   Solution is interpolated at time slices T
%   x_0 and v_0 are initial states of vehicles
%   params correspond to parameters of selected model and are passed to
%   solver
%
%   USE:
%   [X, V] = simul(...)
%   [sim_q] = simul(...)


n = length(x_0);
simulated_queue = cell(1,n);


if size(params, 1) <= 1
    temp = zeros(n, size(params,2));
    for vhcl = 1:n
        temp(vhcl,:) = params;
    end
    params=temp;
end

% set simulation time
tspan = [min(T),max(T)];

X = zeros(n,numel(T));
V = zeros(n,numel(T));

varargout = cell(1,nargout-1);

%leader
tl = T;
xl = [1000+20*tl; 20*ones(1,numel(tl))];

t_start = 0;

for vhcl = 1:n
    
    options = odeset('RelTol',1e-6);
    if model == "IDM"
        s = ode45(@(t,x) IDM(t, x, tl, xl, params(vhcl,:)), tspan, [x_0(vhcl);v_0(vhcl)], options);
    elseif model == "IIDM"
        s = ode45(@(t,x) IIDM(t, x, tl, xl, params(vhcl,:)), tspan, [x_0(vhcl);v_0(vhcl)], options);
    elseif model == "IIDM_WD"
        s = ode45(@(t,x) IIDM_WD(t, x, tl, xl, params(vhcl,:)), tspan, [x_0(vhcl);v_0(vhcl)], options);
    elseif model == "LTI"
        s = ode45(@(t,x) LTI(t, x, t_start, params(vhcl,:)), tspan, [x_0(vhcl);0], options);
        t_start = t_start + params(vhcl,2);
    end
    y = deval(s,T);
    simulated_queue{vhcl}.X = y(1,:);
    simulated_queue{vhcl}.V = y(2,:);
    simulated_queue{vhcl}.T = T;
    
    X(vhcl,:) = y(1,:);
    V(vhcl,:) = y(2,:);
    
    tl = T;
    xl = y;
end

if nargout > 1
    varargout{1} = X;
    varargout{2} = V;
else
    varargout{1} = simulated_queue;
end

end

