function dxdt = LTI(t, x, t_s, params)
% Deffinition of IDM
%parameteres:
%   input:
%       t = time [s]
%       x = states [position;velocity]
%       x_l - states of leading vehicle at times t_l
%   params: [v_0, T, a, b, delta, l, s_0] (1x6)
%       v_0 = desired velocity [m/s]
%       T = desired time headway [s]
%       a = maximum desired acceleration [m/s^2]
%       b = maximum desired deceleration [m/s^2]
%       l = leader's car length [m]
%       s_0 = minimim linear bumper-to-bumper distance
%   output:
%       dxdt = state derivatives [velocity;acceleration]
        dxdt = [0;0];
        v_0 = params(1);
        T = params(2);
        a = params(3);
        
        
        if t >= T+t_s
            dxdt(1) = x(2);
            dxdt(2) = a*(1 - x(2)/v_0);
        end
end