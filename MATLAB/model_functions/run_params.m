function [F] = run_params(params, T, prev_params, model, x0, v0)
%RUN_PARAMS simulates a given model for parameter optimization
[X,V] = simul(model,x0, v0, [prev_params; params], T);
F=real([X(numel(x0), :)'; V(numel(x0), :)']);
end

