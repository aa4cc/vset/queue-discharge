function dxdt = IIDM(t, x, t_l, x_l, params)
% Deffinition of IDM
%parameteres:
%   input:
%       t = time [s]
%       x = states [position;velocity]
%       x_l - states of leading vehicle at times t_l
%   params: [v_0, T, a, b, delta, l, s_0] (1x6)
%       v_0 = desired velocity [m/s]
%       T = desired time headway [s]
%       a = maximum acceleration [m/s^2]
%       b = maximum deceleration [m/s^2]
%       l = leader's car length [m]
%       s_0 = minimim linear bumper-to-bumper distance
%   output:
%       dxdt = state derivatives [velocity;acceleration]
        dxdt = [0;0];
        v_0 = params(1);
        T = params(2);
        a = params(3);
        b = params(4);
        l = params(5);
        s_0 = params(6);
        delta = 4;
        
        s = interp1(t_l, x_l(1,:), t) - x(1) - l;
        dv = x(2)-interp1(t_l, x_l(2,:), t);
        
        s_star = s_0 + max(0, x(2)*T+x(2)*dv/(2*sqrt(a*b)));
        dxdt(1) = x(2);
        z = s_star/s;
        
        
        if x(2) < v_0
            a_free = a*(1-(x(2)/v_0)^delta);
            if z >= 1
                dxdt(2) = a*(1-z^2);
            else
                dxdt(2) = a_free*(1-z^(2*a/a_free));
            end
        else
            a_free = -b*(1-(v_0/x(2))^(a*delta/b));
            if z>= 1
                dxdt(2) = a_free + a*(1-z^2);
            else
                dxdt(2) = a_free;
            end
        end
        
        if real(z) > 1 && real(dxdt(1)) <= 0
            dxdt(1) = 0;
            dxdt(2) = max(0, real(dxdt(2)));
        end
        if norm(imag(dxdt)) > 0
            sprintf("Error: complex number in IIDM")
        end
            
end