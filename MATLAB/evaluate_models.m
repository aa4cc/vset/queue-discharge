%% Script to compute accuracy of all models
addpath('data_functions');
addpath('model_functions');

REFIT = false;  % do you want to refit all the parameters ?

MODELS = ["IDM", "IIDM", "LTI"];
MODE = "SEP"; % "AVG" or "SEP"
INTERSECTIONS = 1:2;
LANES = 1:40;

EXCLUDE = zeros(length(INTERSECTIONS), length(LANES));

STOP_TIME = 5;    %how long a car needs to go very slowly to be considered a part of a queue
MIN_QUEUE_LEN = 3; 


% manually exclude queues with unfit trajectories
% turning lanes:
EXCLUDE(1,6) = 1;
EXCLUDE(1,8) = 1;
EXCLUDE(1,9) = 1;
EXCLUDE(1,11) = 1;
EXCLUDE(1,12) = 1;
EXCLUDE(1,13) = 1;

EXCLUDE(2,11) = 1;
EXCLUDE(2,12) = 1;
EXCLUDE(2,13) = 1;
EXCLUDE(2,14) = 1;
EXCLUDE(2,19) = 1;
%broken data
EXCLUDE(2,20) = 1;
EXCLUDE(2,15) = 1;


% IDM PARAMS
PARAMS{1}.param_0 =  [13,     1,      2,      2,      4,      2];  %initial guess for the parameters [v_0,T,a,b,l,s_0, delta]
PARAMS{1}.param_lb = [10,     0.1,    1,      1,      3,      0.5];  %lower bound on parameters
PARAMS{1}.param_ub = [18,     3,      5,      5,      7,      10];  %upper bound on parameters
% IIDM PARAMS
PARAMS{2} = PARAMS{1};
% LTI PARAMS
PARAMS{3}.param_0 =  [15,       1,      3];  %initial guess for the parameters [v_0,T,a]
PARAMS{3}.param_lb = [14,       0.1,    0.1];  %lower bound on parameters
PARAMS{3}.param_ub = [16,       3,      10];  %upper bound on parameters
% IIDM_WD PARAMS
PARAMS{4}.param_0 =  [PARAMS{1}.param_0,    0.1];  %initial guess for the parameters [v_0,T,a,b,l,s_0, D]
PARAMS{4}.param_lb = [PARAMS{1}.param_lb,   0];  %lower bound on parameters
PARAMS{4}.param_ub = [PARAMS{1}.param_ub,   0.5];    %upper bound on parameters   

%% Figure formating
close all;
out_dir = "../pictures/evaluate_models/";
f = figure('Position', [1,1,600,600]);
out_format1 = 'epsc';
out_format2 = 'png';
fontsize = 18;
linewidth = 2;
MODEL_NAMES = ["IDM", "IIDM", "LQDM"];

%% Extract real queues

sprintf("starting queue extraction")
t1 = tic;
QS = {};

n_iters = length(INTERSECTIONS)*length(LANES);
n_queues = 0;
n = 0;
for intersection = INTERSECTIONS
    A = read_data(intersection);
    for lane = LANES
        if EXCLUDE(intersection, lane)
            continue
        end
        if mod(floor(100*n/(n_iters-1)),5) == 0
            sprintf("\r elapsed time: %.2f s, %.0f %% complete", toc(t1), 100*n/(n_iters-1))
        end
        n = n + 1;
        lane_data = get_lane(A, intersection, lane);    %extracts only cars from selected lane
        if isempty(lane_data)
            continue
        end
        lane_data = filter_lane(lane_data, STOP_TIME);  %filters cars that are part of a queue
        queues = split_queues(lane_data);               %splits queues so they can be analyzed separately
        k = 1;
        for q = 1:length(queues)
            if length(queues{q}) >= MIN_QUEUE_LEN
                QS{intersection}{lane}{k} = queues{q};
                k = k+1;
                n_queues = n_queues + 1;
            end
        end
    end
end
t2 = toc(t1);
sprintf("queues extracted, elapsed time : %.2f s", t2)


%% Fitting
if REFIT
    sprintf("starting fitting")
    t1 = tic;
    options = optimoptions('lsqcurvefit', 'Algorithm', 'levenberg-marquardt','ScaleProblem', 'jacobian', 'InitDamping', 1e-10);
    options = optimoptions(options,'Display', 'none');


    n_iters = n_queues*length(MODELS);
    n = 0;
    for intersection = 1:length(QS)
        for lane = 1:length(QS{intersection})
            for q=1:length(QS{intersection}{lane})
                real_queue = QS{intersection}{lane}{q};
                real_q_len = length(real_queue);
                x0 = zeros(real_q_len,1);
                v0 = zeros(real_q_len,1);
                Tend = zeros(real_q_len,1);

                for model = 1:length(MODELS)
                    sprintf("\r elapsed time: %.2f s, %.2f %% complete", toc(t1), 100*n/(n_iters-1))
                    n = n + 1;

                    prev_params = [];
                    for vhcl = 1:real_q_len
                        x0(vhcl) = real_queue{vhcl}.X(1);
                        v0(vhcl) = real_queue{vhcl}.V(1);
                        X = real_queue{vhcl}.X';
                        V = real_queue{vhcl}.V';
                        T = real_queue{vhcl}.T - real_queue{vhcl}.T(1);
                        Y = [X;V];
                        best_params = lsqcurvefit(@(params, t) run_params(params, t, prev_params, MODELS(model), x0(1:vhcl), v0(1:vhcl)), PARAMS{model}.param_0, T, Y, PARAMS{model}.param_lb, PARAMS{model}.param_ub, options);
                        prev_params = [prev_params;best_params];
                    end
                    OPT_PARAMS{intersection}{lane}{q}{model} = prev_params;
                end

            end
        end
    end
    save("./model_parameters/opt_params.mat",'OPT_PARAMS');
    t2 = toc(t1);
    sprintf("model parameters fitted, elapsed time : %.2f s", t2)

else
    OPT_PARAMS = load("./model_parameters/opt_params.mat").OPT_PARAMS;
end

%% Simulate using optimal parameters

sprintf("starting simulations")
t1 = tic;
n_iters = n_queues*length(MODELS);
n = 0;

SQS = {};
for intersection = 1:length(OPT_PARAMS)
    for lane = 1:length(OPT_PARAMS{intersection})
        if EXCLUDE(intersection, lane)
            continue
        end
        for q = 1:length(OPT_PARAMS{intersection}{lane})
            if MODE == "SEP"
                queue_params = OPT_PARAMS{intersection}{lane}{q};
                if isempty(queue_params)
                    continue
                end
            end
            real_queue = QS{intersection}{lane}{q};
            real_q_len = length(real_queue);
            x_0 = zeros(real_q_len,1);
            v_0 = zeros(real_q_len,1);
            T = real_queue{end}.T - real_queue{end}.T(1);
            for vhcl=1:real_q_len
                x_0(vhcl) = real_queue{vhcl}.X(1);
                v_0(vhcl) = real_queue{vhcl}.V(1);
            end
            for model = 1:length(MODELS)
                if MODE == "AVG"
                    temp = OPT_PARAMS{intersection}{lane};
                    opt_params = [];
                    for q=1:length(temp)
                        opt_params = [opt_params; temp{q}{model}];
                    end
                    opt_params = mean(opt_params,1);
                    if MODELS(model) ~= "LTI"
                        temp = zeros(real_q_len, numel(opt_params));

                        temp(1,:) = opt_params;
                        for vhcl=2:real_q_len
                            temp(vhcl,:) = opt_params;
                            temp(vhcl,6) = min(opt_params(6), x_0(vhcl-1) - opt_params(5) - x_0(vhcl));
                        end
                        opt_params = temp;
                    end
                    queue_params{model} = opt_params;
                    if isempty(queue_params)
                        continue
                    end
                end
                
                sprintf("\r elapsed time: %.2f s, %.2f %% complete", toc(t1), 100*n/(n_iters-1))
                n = n + 1;
                SQS{intersection}{lane}{q}{model} = simul(MODELS(model), x_0, v_0, queue_params{model}, T);
            end
        end
    end
end
t2 = toc(t1);
sprintf("simulations finished, elapsed time : %.2f s", t2)


%% Error computation


x_error = [];
v_error = [];
k = 0;
for intersection = 1:length(SQS)
    for lane = 1:length(SQS{intersection})
        for q = 1:length(SQS{intersection}{lane})
            if isempty(SQS{intersection}{lane}{q})
                continue
            end
            real_q = QS{intersection}{lane}{q};
            x_err = zeros(length(real_q), length(MODELS));
            v_err = zeros(length(real_q), length(MODELS));
            for model = 1:length(MODELS)
                sim_q = SQS{intersection}{lane}{q}{model};
                if length(sim_q) ~= length(real_q)
                    sprintf("ERROR: real and simulated queue are not of the same length");
                end
                for vhcl = 1:length(sim_q)
                    k = k+1;
                    elems = min(numel(sim_q{vhcl}.T),numel(real_q{vhcl}.T)); 
                    x_diff = sim_q{vhcl}.X(1:elems) - real_q{vhcl}.X(1:elems);
                    v_diff = sim_q{vhcl}.V(1:elems) - real_q{vhcl}.V(1:elems);
                    x_err(vhcl, model) = sqrt(sum(x_diff.^2)/numel(x_diff));
                    v_err(vhcl, model) = sqrt(sum(v_diff.^2)/numel(v_diff));
                    if false
                        sprintf("HERE");
                    end
                    
                end
            end
            x_error = [x_error; x_err];
            v_error = [v_error; v_err];
        end
    end
end

x_err_mean = mean(x_error,1);
x_err_sig = sqrt(var(x_error,0,1));
v_err_mean = mean(v_error,1);
v_err_sig = sqrt(var(v_error,0,1));

%% Visualizetion - Position accuracy
clf;
boxplot(x_error, 'DataLim', [0,25], 'ExtremeMode', 'compress', 'Notch', 'on');
ylabel("$\mathrm{RMS}\left(\Delta \textbf{x}\right) \left[\mathrm{m}\right]$", 'Interpreter', 'latex', 'FontSize', fontsize);
set(gca,'XTickLabel',MODEL_NAMES, 'FontSize', fontsize, 'TickLabelInterpreter', 'latex');
grid on;
InSet = get(gca, 'TightInset');
set(gca, 'Position', [InSet(1:2), 1-InSet(1)-InSet(3), 1-InSet(2)-InSet(4)]);
saveas(f,out_dir + MODE + "_ALL_VEH_X_RMSE", out_format1);
saveas(f,out_dir + MODE + "_ALL_VEH_X_RMSE", out_format2);

%% Visualization - Velocity accuracy
clf
boxplot(v_error, 'ExtremeMode', 'compress', 'Notch', 'on');
ylabel("$\mathrm{RMS}\left(\Delta \textbf{v}\right) \left[\mathrm{ms^{-1}}\right]$", 'Interpreter', 'latex', 'FontSize', fontsize);

set(gca,'XTickLabel',MODEL_NAMES, 'FontSize', fontsize, 'TickLabelInterpreter', 'latex');
grid on;
InSet = get(gca, 'TightInset');
set(gca, 'Position', [InSet(1:2), 1-InSet(1)-InSet(3), 1-InSet(2)-InSet(4)]);

saveas(f,out_dir + MODE + "_ALL_VEH_V_RMSE", out_format1);
saveas(f,out_dir + MODE + "_ALL_VEH_V_RMSE", out_format2);

%% Velocity of last vehicles in queues

v_l_error = [];
k = 0;
for intersection = 1:length(SQS)
    for lane = 1:length(SQS{intersection})
        v_l_err = zeros(length(SQS{intersection}{lane}), length(MODELS));
        for q = 1:length(SQS{intersection}{lane})
            if isempty(SQS{intersection}{lane}{q})
                continue
            end
            real_q = QS{intersection}{lane}{q};
            for model = 1:length(MODELS)
                sim_q = SQS{intersection}{lane}{q}{model};
                if length(sim_q) ~= length(real_q)
                    sprintf("ERROR: real and simulated queue are not of the same length");
                end
                k = k+1;
                elems = min(numel(sim_q{end}.T),numel(real_q{end}.T));
                v_diff = sim_q{end}.V(1:elems) - real_q{end}.V(1:elems);
                v_l_err(q, model) = sqrt(sum(v_diff.^2)/numel(v_diff));     
            end
            v_l_error = [v_l_error; v_l_err];
        end
    end
end
v_l_err_mean = mean(v_l_error,1);
v_l_err_sig = sqrt(var(v_l_error,0,1));

%% visualization
clf;
boxplot(v_l_error, 'DataLim', [0,25], 'ExtremeMode', 'compress', 'Notch', 'on');
grid on;
ylabel("$\mathrm{RMS}\left(\Delta \textbf{v}\right) \left[\mathrm{ms^{-1}}\right]$", 'Interpreter', 'latex', 'FontSize', fontsize);
set(gca,'XTickLabel',MODEL_NAMES, 'FontSize', fontsize, 'TickLabelInterpreter', 'latex');
InSet = get(gca, 'TightInset');
set(gca, 'Position', [InSet(1:2), 1-InSet(1)-InSet(3), 1-InSet(2)-InSet(4)]);
grid on;
saveas(f,out_dir + MODE + "_LAST_VEH_V_RMSE", out_format1);
saveas(f,out_dir + MODE + "_LAST_VEH_V_RMSE", out_format2);


    
               
            
            

