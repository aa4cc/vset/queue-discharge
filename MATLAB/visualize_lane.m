%%
% Script to view collected data with computed velocities and accelerations,
% all data is transformed to represent a distance from the traffic light.
format long;
close all;

addpath('data_functions');

INTERSECTION = 2; %choose intersection 
LANE = 15;        %choose lane

out_dir = "../pictures/visualize_lane/";
f = figure('Position', [1,1,800,400]);
out_format1 = 'epsc';
out_format2 = 'png';
fontsize = 15;
linewidth = 2;

%% 
% computes data

A = read_data(INTERSECTION);            %reads raw data
lane = get_lane(A, INTERSECTION, LANE); %extracts only cars from selected lane
lane_len = length(lane);                %number of cars in the lane

%%
% plots data

clf;
hold on;
for vhcl=1:lane_len
    plot(lane{vhcl}.T, lane{vhcl}.X, 'LineWidth', 2);
end
hold off;
grid;
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$x [m]$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_POS", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_POS", out_format2); 
%%
clf;
hold on;
for vhcl=1:lane_len
    plot(lane{vhcl}.T(2:end), lane{vhcl}.V, 'LineWidth', 2);
end
hold off;
grid;
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$v \left[ \frac{m}{s}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_VEL", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_VEL", out_format2); 

%%
clf;
hold on;
for vhcl=1:lane_len
    plot(lane{vhcl}.T(3:end), lane{vhcl}.A, 'LineWidth', 2);
end
hold off;
grid;
xlabel('$t [s]$', 'Interpreter', 'latex', 'FontSize', fontsize);
ylabel('$a \left[ \frac{m}{s^2}\right]$', 'Interpreter', 'latex', 'FontSize', fontsize);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_ACC", out_format1);
saveas(f,out_dir + "I_" + num2str(INTERSECTION, '%d') + "_L_" + num2str(LANE, '%d') + "_ACC", out_format2); 





