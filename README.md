# Modeling of traffic queue discharge dynamics

![priority vehicle approaching](./pictures/p_opt_sim.png "traffic light preemption for emergency vehicles")

## Intro

This project aims to find a suitable solution for estimation of the time it takes for a queue of vehicles at a traffic light intersection to cross, or reach a desired velocity. Additionally, it focuses on traffic light preemption for emergency vehicles.

## Folders

### [MATLAB](./MATLAB/) 
- Scripts for fitting various car following models on real-traffic data from [DataFromSky](https://datafromsky.com/). 
- First phase of the project.
- Included scripts for preemption are now obsolete - new methods were developed later in the project. 

### [SUMO](./SUMO/) 
- SUMO scenario with python scripts using TraCI to simulate developed preemption schemes. 
- This can be thought of as a second phase of the project.

### [pospichal\_diploma\_thesis](./pospichal_diploma_thesis/) 
- contains latex source code for the master thesis of Lukáš Pospíchal in traffic light preemption for emergency vehicles.

### [report](./report/) 
- contains latex files for the report done to summarize the results of the first phase of the project.

### [pictures](./pictures/)
- MATLAB scripts export saved figures into this directory
- Various pictures from all stages of the project can be found here
- not very well maintained

