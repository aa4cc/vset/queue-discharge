import os, sys
import traci
import traci.constants as tc
import random
import time
import optparse
from sumolib import checkBinary
import numpy as np
import multiprocessing

from preemptionlib.scheduler import Scheduler
from preemptionlib.models import Probabilities
from preemptionlib.visualize import ProgressBar, BoxChart

N_SIMS = 30
N_WORKERS = 4

MODES = ["preemption", "radius", "recomp", "none"]
MODE_NUM = 0 	# select from MODES

TRAFFIC = "MODERATE"

SIGN_IN_DISTANCE = 500

FLAVOR =  TRAFFIC + ("_%d" %SIGN_IN_DISTANCE) # a result specifier

BASE_CAR_PH = 1500
BASE_BUS_PH = 60

if TRAFFIC == "LIGHT":
	CAR_PER_HOUR = BASE_CAR_PH
	BUS_PER_HOUR = BASE_BUS_PH
elif TRAFFIC == "MODERATE":
	CAR_PER_HOUR = BASE_CAR_PH*1.5
	BUS_PER_HOUR = BASE_BUS_PH*1.25
elif TRAFFIC == "HEAVY":
	CAR_PER_HOUR = BASE_CAR_PH*2
	BUS_PER_HOUR = BASE_BUS_PH*1.5
else:
	print("SELECT VALID TRAFFIC FLOW DENSITY")

AMB_DEPART = 300
AMB_DEPART_SKIP = 3
TL_PLAN_LEN = 90


AMB_SOURCE = 0
AMB_SINK = 2

SOURCES = ['w2sep', 'n2sep', 'e2sep', 's2sep']
SINKS = ['c2w', 'c2n', 'c2e', 'c2s']

P_SOURCE = [0.25, 0.25, 0.25, 0.25]
P_SINK = [1.0/3, 1.0/3, 1.0/3, 1.0/3]

def generate_config(name):
	with open("sumofiles/junction_%s.sumocfg" %name, "w") as config:
		print("""
<configuration>
	<input>
		<net-file value="junction.net.xml"/>
		<route-files value="junction_%s.rou.xml"/>
		<gui-settings-file value="junction-gui.settings.xml"/>
		<additional-files value="junction.det.xml"/>
	</input>
	<output>
		<tripinfo value="output/tripinfo_%s.xml"/>
		<!-- other outputs -->
	</output>
	<time>
		<begin value="0"/>
		<end value="10000"/>
		<step-length value="1.0"/>
	</time>
	<processing>
		<!-- sublane model -->
		<lateral-resolution value="0.1"/>
	</processing>
</configuration>
""" %(name,name), file=config)
		
def generate_routefile(seed, probs, start, name):
	random.seed(seed)
	start = int(start)
	max_t = start + 300 
	
	with open("sumofiles/junction_%s.rou.xml" %name, "w") as routes:
		
	# vType definitions --->
		print("""<routes>
	<vType id="Car" 		type="passenger" 	vClass="passenger"	length="3.5"	width="2.0"	color="0,100,255" 	minGapLat="0.5"		guiShape="passenger/sedan"	laneChangeModel="SL2015"	carFollowModel="IDM" />
	<vType id="Bus"			type="bus"			vClass="bus"		length="12.0"	width="2.0"	color="yellow"		minGapLat="0.5"		guiShape="bus"				laneChangeModel="SL2015"	carFollowModel="IDM" />
	<vType id="Ambulance" 	type="emergency"	vClass="emergency"	length="6.5" 	width="2.0"	color="blue"		minGapLat="0.5"		guiShape="emergency"		laneChangeModel="SL2015"	carFollowModel="IDM"	speedFactor="1.5"	jmDriveAfterRedTime="1000.0"	jmDriveAfterYellowTime="1000.0"	jmDriveAfterRedSpeed="5.56"	jmIgnoreFoeProb="1.0"	jmIgnoreFoeSpeed="11.1"	jmIgnoreJunctionFoeProb="1.0" > 
		<param key="has.bluelight.device" value="true"/>
		<param key="device.bluelight.reactiondist" value="100.0"/>
	</vType>""", file=routes)
	
	# <--- vType definitions
		
	# trip definitions --->
		for t in range(max_t):
			for source in range(len(SOURCES)):
				for sink in range(len(SINKS)):
					if sink==source:
						continue
					if probs['Car', SOURCES[source], SINKS[sink]] >= random.random():
						print('	<trip id="car_%s2%s_%d" type="Car" from="%s" to="%s" depart="%d" departSpeed="13.9"/>' % (SOURCES[source][0], SINKS[sink][-1], t, SOURCES[source], SINKS[sink], t), file=routes)
					if probs['Bus', SOURCES[source], SINKS[sink]] >= random.random():
						print('	<trip id="bus_%s2%s_%d" type="Bus" from="%s" to="%s" depart="%d" departSpeed="13.9"/>' % (SOURCES[source][0], SINKS[sink][-1], t, SOURCES[source], SINKS[sink], t), file=routes)
			if t == start:
				print('	<trip id="amb_%s2%s_%d" type="Ambulance" from="%s" to="%s" depart="%d" departSpeed="13.9"/>' % (SOURCES[AMB_SOURCE][0], SINKS[AMB_SINK][-1], t, SOURCES[AMB_SOURCE], SINKS[AMB_SINK], t), file=routes)
	# <--- trip definitions 
	
		print("</routes>", file=routes)

	
def generate_probs():
	vehs = ['Car', 'Bus']
	p_car = CAR_PER_HOUR/3600
	p_bus = BUS_PER_HOUR/3600
	probs = Probabilities(vehs, SOURCES, SINKS)
	for source in range(len(SOURCES)):
		for sink in range(len(SINKS)):
			if sink==source:
				continue
			probs['Car', SOURCES[source], SINKS[sink]] = p_car*P_SOURCE[source]*P_SINK[sink]
			probs['Bus', SOURCES[source], SINKS[sink]] = p_bus*P_SOURCE[source]*P_SINK[sink]
	return probs
	
def run(mode, seed, start, probs, gui=False, verbose=False, visualize=False):
	print("MODE: %s, seed: %d, start: %d" %(mode, seed, start))
	t1 = time.time()
	name = "%s_%s" %(mode,seed)
	if gui:
		sumoBinary = checkBinary('sumo-gui')
		sumoCmd = [sumoBinary, "-c", "sumofiles/junction_%s.sumocfg" %name, "-S", "--no-step-log", "true", "--no-warnings","true","--error-log", "sumofiles/output/errorlog_%s.log" %name]
	else:
		sumoBinary = checkBinary('sumo')
		sumoCmd = [sumoBinary, "-c", "sumofiles/junction_%s.sumocfg" %name, "--no-step-log", "true", "--no-warnings","true","--error-log", "sumofiles/output/errorlog_%s.log" %name]
	generate_config(name)
	generate_routefile(seed, probs, start, name)
	traci.start(sumoCmd)
	shed = Scheduler("center", mode, radius=SIGN_IN_DISTANCE, probabilities = probs, verbose=verbose, visualize=visualize)
	traci.addStepListener(shed)
	
	ret1 = 0
	ret2 = 0
	ret3 = 0
	amb_in = False
	while traci.simulation.getMinExpectedNumber() > 0:
		traci.simulationStep()
		if len(shed.stall_vehs) > 0:
			amb_in = True
		if amb_in and len(shed.stall_vehs) == 0:
			ret1 = shed.avg_stall
			ret2 = shed.amb_stall
			ret3 = shed.amb_min_vel
			break
	traci.close()
	t2 = time.time()
	os.remove("sumofiles/junction_%s.sumocfg" %name)
	os.remove("sumofiles/junction_%s.rou.xml" %name)
	#print("mode: %s, seed: %d, start: %d --- DONE: %d s" %(mode, seed, start, t2-t1))
	sys.stdout.flush()
	return ret1, ret2, ret3
			
	

# this is the main entry point of this script
if __name__ == "__main__":
	
	bar = ProgressBar()
	veh_box = BoxChart(labels=[MODES[MODE_NUM]], title="vehicle delay")
	amb_box = BoxChart(labels=[MODES[MODE_NUM]], title="minimum EV speed")
	
	bar.updateBar(0, 0)
	
	
	start_times = range(AMB_DEPART, AMB_DEPART+TL_PLAN_LEN+1, AMB_DEPART_SKIP)
	seeds = range(N_SIMS)
	probs = generate_probs()
	veh_stalls = np.zeros((len(start_times), len(seeds)))
	amb_stalls = np.zeros((len(start_times), len(seeds)))
	amb_vels = np.zeros((len(start_times), len(seeds)))
	
	veh_box.updateChart(veh_stalls.flatten())
	amb_box.updateChart(amb_vels.flatten())
	
	start = time.time()
	jobsdone = 0
	njobs = len(start_times)
	
	for k in range(len(start_times)):
		args = []
		for l in range(len(seeds)):
			args.append((MODES[MODE_NUM], seeds[l], start_times[k], probs)) 
		with multiprocessing.Pool(processes=N_WORKERS) as pool:
			results = pool.starmap_async(run, args)
			results.wait()
		
		res = np.array(results.get())
		veh_stalls[k, :] = res[:,0]
		amb_stalls[k, :] = res[:,1]
		amb_vels[k,:] = res[:,2]
		
		# print progress
		jobsdone += 1
		t_togo = (time.time()-start)*(njobs/jobsdone-1)
		bar.updateBar(jobsdone/njobs, t_togo)
		
		veh_box.updateChart(veh_stalls[:k+1,:].flatten())
		amb_box.updateChart(amb_vels[:k+1,:].flatten())
			
	# test this
	
	
	
	with open("results/veh_%s_%s_%s2%s.npy" %(FLAVOR, MODES[MODE_NUM], SOURCES[AMB_SOURCE][0], SINKS[AMB_SINK][-1]), 'wb') as f:
		np.save(f, veh_stalls)
	with open("results/ev_%s_%s_%s2%s.npy" %(FLAVOR, MODES[MODE_NUM], SOURCES[AMB_SOURCE][0], SINKS[AMB_SINK][-1]), 'wb') as f:
		np.save(f, amb_stalls)
	with open("results/speed_%s_%s_%s2%s.npy" %(FLAVOR, MODES[MODE_NUM], SOURCES[AMB_SOURCE][0], SINKS[AMB_SINK][-1]), 'wb') as f:
		np.save(f, amb_vels)
	
		
	os.system('spd-say "Lukas, your program has finished."')
	
	input("U done ?\tpress ENTER to exit. ")
	
	bar.close()
	veh_box.close()
	amb_box.close()
		
