import numpy as np
from preemptionlib.visualize import BoxChart

DATA_DIR = "results"
SAVE_DIR = "figures"

POS_LABEL = r"$x \, \left[ \mathrm{m} \right] $"
VEL_LABEL = r"$v \, \left[ \mathrm{ms}^{-1} \right] $"
TIME_LABEL = r"$t \, \left[ \mathrm{s}\right] $" 



VEH = "speed"
FLAVOR = "MODERATE"
DISTANCE = 800
MODES =  ["none", "radius",  "preemption",  "recomp", ]
LABELS = [r"none", r"polygon", r"$t_\mathrm{p}^*$ computation", r"$t_\mathrm{p}^*$ recomputation"]
FROM = "w"
TO = "e"

IN_FILES = []

NAME = "VIRT"+VEH

IN_FILES.append(VEH+"_"+"MODERATE"+"_400_radius_"+FROM+"2"+TO)
IN_FILES.append(VEH+"_"+"MODERATE"+"_800_radius_"+FROM+"2"+TO)
IN_FILES.append(VEH+"_"+"MODERATE"+"_800_preemption_"+FROM+"2"+TO)
IN_FILES.append(VEH+"_"+"MODERATE"+"_800_recomp_"+FROM+"2"+TO)
IN_FILES.append(VEH+"_"+"SHORT_MODERATE"+"_800_preemption_"+FROM+"2"+TO)
IN_FILES.append(VEH+"_"+"SHORT_MODERATE"+"_800_recomp_"+FROM+"2"+TO)


LABELS = [r'''$\mathrm{radius}$
(400 m)''', r'''$\mathrm{radius}$
(800 m)''', r'''$t_\mathrm{p}^*$
(LONG)''', r'''re-$t_\mathrm{p}^*$
(LONG)''', r'''$t_\mathrm{p}^*$
(SHORT)''', r'''re-$t_\mathrm{p}^*$
(SHORT)''']



#for mode in MODES:
#	IN_FILES.append(VEH+"_"+FLAVOR+("_%d" %DISTANCE) + "_" + mode +"_"+FROM+"2"+TO)
	
data = []

for f in IN_FILES:
	data.append(np.load(DATA_DIR+"/"+f+".npy").flatten())
	
for k in range(len(data)):
	print("'%s' - data size: ", data[k].shape)
	
	
box = BoxChart(labels=LABELS, ylabel=VEL_LABEL)
medians = box.updateChart(data)

print(MODES)
for median in medians:
	print("%.2f\t" %median, end='')
print("")


box.save(NAME,SAVE_DIR)


input("U done ?\tpress ENTER to exit. ")
