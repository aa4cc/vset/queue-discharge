# Scripts

## [evaluation.py](./evaluation.py)
- Evaluates different preemption methods using SUMO and TraCI
- saves computed vehicle halt times and minimum EV velocities into [results](./results/) in .npy format

### MACROS
- __N\_SIMS__ - sets the number of simulations that are run for each setup using different seed for the random number generator each time
- __N\_WORKERS__ - sets the number of CPUs used  
- __MODES__ - list of methods to evaluate
- __TRAFFIC__ - traffic flow density to be used for the test
- __SIGN\_IN\_DISTANCE__ - distance \[m\] to the intersection at which an EV triggers preemption time computation
- __AMB\_DEPART__ - earliest time \[s\] at which the EV is injected into the simulation
- __AMB\_DEPART\_SKIP__ - time increment \[s\] for EV injection time for different tests 
- __TP\_PLAN\_LEN__ - length \[s\] of the default traffic light signal plan
- __AMB\_SOURCE__ and __AMB\_SINK__ define the path of the EV. They are indices of __SOURCES__ and __SINKS__.

## [simulation.py](./simulation.py)
- Runs a single simulation of the SUMO scenario. Some options need to be edited in [evaluation.py](./evaluation.py) for full customization.

## [makefigures.py](./makefigures.py)
- Makes boxcharts from data in [results](./results/) and saves them into [figures](./figures/).

# Subfolders

## sumofiles
- contains the sumo scenario definition files

## preemptionlib
- python3 library containing the classes and functions used to implement the preemption methods.

## figures
- picture outputs

## results
- evaluation output data
