from evaluation import *



if __name__ == "__main__":
	
	model = "prune"
	start_time = 320
	#0 2 4 6 8 10 .. 28 9 5
	seed = 0
	probs = generate_probs()
	
	veh_stall, amb_stall = run(model, seed, start_time, probs, gui=True, verbose=True, visualize=True)
	
	print("Avg. stall: %.2f" %veh_stall)
