import traci
import numpy as np
import matplotlib.pyplot as plt

class Probabilities():
	def __init__(self, vehs, sources, sinks):
		self.vehicles = vehs
		self.sources = sources
		self.sinks = sinks
		self.map = self.zeroProbMap()
	
	def zeroProbMap(self):
		return np.zeros((len(self.vehicles), len(self.sources), len(self.sinks)))
		
	def __getitem__(self, key):
		return self.map[self.vehicles.index(key[0]),self.sources.index(key[1]), self.sinks.index(key[2])]
		
	def __setitem__(self, key, value):
		self.map[self.vehicles.index(key[0]),self.sources.index(key[1]), self.sinks.index(key[2])] = value
	
	def vehProb(self, veh, source):
		return self.map[self.vehicles.index(veh),self.sources.index(source), :].sum()

class Parameters():
	def __init__(self, vehID, model="IDM"):
		self.model = model
		self.vehID = vehID
		
		if self.model == "IDM" or self.model == "IIDM":
			self.V0 = traci.vehicle.getAllowedSpeed(self.vehID)
			self.a = traci.vehicle.getAccel(self.vehID)
			self.b = traci.vehicle.getDecel(self.vehID)
			self.T = traci.vehicle.getTau(self.vehID)
			self.s0 = traci.vehicle.getMinGap(self.vehID)
			self.l = traci.vehicle.getLength(self.vehID)
		elif self.model == "INIT_IDM" or self.model == "INIT_IIDM":	
			self.V0 = 13.9
			self.a = 0
			self.b = 0
			self.T = 0
			self.s0 = 0
			self.l = 0
		elif self.model == "VIRT_IDM" or self.model == "VIRT_IIDM":	
			self.V0 = 13.9
			self.a = 0
			self.b = 0
			self.T = 0
			self.s0 = 0
			self.l = 0
		elif self.model == "DEBUG":
			self.V0 = 13.9
			self.a = 4
			self.b = 2
			self.T = 1
			self.s0 = 2
			self.l = 4
			

class Model():
	def __init__(self, model = "IDM"):
		
		if model == "IDM":
			self.model = self.IDM
		elif model == "IIDM":
			self.model = self.IIDM
		else:
			mod = self.IDM
		
		if __name__ == "__main__":
			self.dt = 0.1
		else:
			#self.dt = min(traci.simulation.getDeltaT(), 0.05)
			self.dt = traci.simulation.getDeltaT()
			
		self._MAX_OVERTAKE_VEL = 20/3.6	# maximum velocity under which amb tries to overtake instead of follow
		
		
	def IDM(self, t, x, x_l, params):
		
		dxdt = np.zeros((2,1))
		
		v_0 = params.V0
		T = params.T
		a = params.a
		b = params.b
		l = params.l
		s_0 = params.s0
		delta = 4
		
		s_star = s_0 + max(0, x[1]*T+x[1]*(x[1]-x_l[1])/(2*np.sqrt(a*b)))
		dxdt[0] = x[1]
		dxdt[1] = a*(1-(x[1]/v_0)**delta-(s_star/s)**2)
		
		return dxdt
		
	def IIDM(self, t, x, x_l, params):
		
		dxdt = np.zeros((2,1))
		
		v_0 = params.V0
		T = params.T
		a = params.a
		b = params.b
		l = params.l	# leader's length
		s_0 = params.s0
		delta = 4
		
		x[1] = max(0,x[1])
					
		s = x_l[0] - x[0] - l
		dv = x[1] - x_l[1]
		
		s_star = s_0 + max(0, x[1]*T+x[1]*dv/(2*np.sqrt(a*b)))
		dxdt[0] = x[1]
		z = s_star/s
		if x[1] <= v_0:
			a_free = a*(1-(x[1]/v_0)**delta)
			if z >= 1:
				dxdt[1] = a*(1-z**2)
			elif a_free == 0:
				dxdt[1] = 0
			else:
				dxdt[1] = a_free*(1-z**(2*a/a_free))
		else:
			a_free = -b*(1-(v_0/x[1])**(a*delta/b))
			if z>= 1:
				dxdt[1] = a_free + a*(1-z**2)
			else:
				dxdt[1] = a_free
				
		if abs(z) > 1 and x[1] <= 0:
			dxdt[0] = 0
			dxdt[1] = max(0,dxdt[1]) 
				
		if np.any(np.isnan(dxdt)) or np.any(np.isinf(dxdt)) or np.any(np.abs(dxdt) > 2000):
			print("\n ERROR: IIDM computed NAN or inf")
			print("\n-------")
			print("ID: ", params.vehID)
			print("t: ", t)
			print("x: ", x[0])
			print("v: ", x[1])
			print("v0: ", v_0)
			print("xl: ", x_l[0])
			print("vl: ", x_l[1])
			print("s: ", s)
			print("s0: ", s_0)
			print("s*: ", s_star)
			print("z: ", z)
			print("a_free: ",a_free)
			print("a_comp: ", dxdt[1])
			
			
			print("-------\n")
			input()
		
		return dxdt
		
	def simulate(self, x0, tspan, xl, params, lights = None):
		xs = np.zeros(xl.shape)
		xs[:,0] = x0
		#print("x\t|v\t|t")
		#print("%.2f\t|%.2f\t|%.2f" % (xs[0,0], xs[1,0], tspan[0]))
		for n in range(1,tspan.size):
			if lights != None:
				xs[:,n] = self.rk4(tspan[n-1], xs[:,n-1], xl[:,n-1], params, lights[k])
			else:
				xs[:,n] = self.rk4(tspan[n-1], xs[:,n-1], xl[:,n-1], params)
			#print("%.2f\t|%.2f\t|%.2f" % (xs[0,n], xs[1,n], tspan[n]))
		return xs
		
	def discharge(self, x0, tspan, params, lights = None):
		n = x0.shape[1]
		xs = np.zeros((n, tspan.size))
		vs = np.zeros((n, tspan.size))
		xs[:,0] = x0[0,:]
		vs[:,0] = x0[1,:]
		
		xl =  np.zeros((2,2))
		x = np.zeros((2,1))
		
		kmax = 0
		for k in range(1,tspan.size):
			for vhcl in range(n):
				
				x[0] = xs[vhcl,k-1]
				x[1] = vs[vhcl,k-1]
				
				if vhcl == 0:
					xl[0,:] = 1000 + x[0]
					xl[1,:] = 0
				else:
					xl[0,:] = xs[vhcl-1, k-1:k+1]
					xl[1,:] = vs[vhcl-1, k-1:k+1]
				
				
				if lights != None:
					sol = self.rk4(tspan[k-1], x, xl, params[vhcl], lights[k-1])
				else:
					sol = self.rk4(tspan[k-1], x, xl, params[vhcl])
					
				xs[vhcl,k] = sol[0]
				vs[vhcl,k] = sol[1]
			
			if xs[-1,k] > 0:
				break
			kmax+=1
		return xs[:,:kmax], vs[:,:kmax], tspan[:kmax]  
			
		
	def euler(self, t, x, xl, params, light):
		
		x = x.reshape((2,1))
		xl = xl.reshape((2,1))
		
		ret = self.dt*self.model(t, x, xl, params, light) + x
		return  ret.reshape((2,))
		
	def rk4(self, t, x, xl, params, light):
		
		col = None
		if light != None:
			if light == 'y' or light == 'Y':
				col = 'Y'
			elif light == 'r' or light == 'R':
				col = 'R'
			elif light == 'g' or light == 'G':
				col = 'G'
			else:
				print("Unknown light signal")
				col = None
				
			if (col == 'R' or (col == 'Y' and x[0] + x[1]**2/(2*params.b) <= 0)) and x[0] <= 0 and np.any(xl[0,:] > params.l):
				xl[0,:] = params.l + params.s0
				xl[1,:] = 0
		
		x = x.reshape((2,1))
		k1 = self.model(t, x, xl[:,0], params)
		k2 = self.model(t+self.dt/2, x + self.dt*k1/2, xl.mean(axis=1,keepdims=True), params)
		k3 = self.model(t+self.dt/2, x + self.dt*k2/2, xl.mean(axis=1,keepdims=True), params)
		k4 = self.model(t+self.dt, x + self.dt*k3, xl[:,1], params)
		ret = x + 1/6*self.dt*(k1 + 2*k2 + 2*k3 + k4)
		
		if (col == 'R' or (col == 'Y' and x[0] + x[1]**2/(2*params.b) <= 0)) and x[0] <= 0 and ret[0] > 0:
			#print("Vehicle ran a red light: x(t)=%.2f, x(t+1)=%.2f, diff = %.4f" %(x[0], ret[0], ret[0]-x[0]))
			ret[0] = 0
		return  ret.reshape((2,))
		
if __name__ == "__main__":
	# THIS IS A FUNCTIONALITY TEST SPACE
	# NO OTHER CODE SHOULD BE RUN FROM HERE
	N = 15
	pos_init = list(range(0,-N*10, -10))
	vel_init = [0]*len(pos_init)
	
	pos_init[-1] = -500
	vel_init[-1] = 20
	
	pos_init[-2] = (pos_init[-3] + pos_init[-1])/2
	vel_init[-2] = 13
	
	
	x0 = np.array([pos_init, vel_init])
	params = Parameters("TST", "DEBUG")
	
	mod = Model("IIDM")
	tspan = np.arange(0,60+mod.dt,mod.dt)
	
	xl = np.zeros((2, tspan.size))
	xl[0,:int(tspan.size/3)] = 5
	xl[0,int(tspan.size/3):] = 500
	
	xs = np.zeros(tspan.shape)
	vs = np.zeros(tspan.shape)
	
	for vhcl in range(N):
		if vhcl == N-1:
			params.V0 = vel_init[N-1]
		sol = mod.simulate(x0[:,vhcl], tspan, xl, params)
		xl = sol
		xs = np.vstack((xs,sol[0,:]))
		vs = np.vstack((vs,sol[1,:]))
	
	mod.visualize(xs[1:,:], vs[1:,:], tspan)
	plt.show(block=True)

	
	
		
		
		
		
