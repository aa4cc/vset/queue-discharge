import traci
import numpy as np

SWITCH_TIME = 3		# time for which a yellow signal is shown during switching
MIN_SIGNAL_TIME = 5 # minimum time the signal has to hold 

class TrafficLight(traci.StepListener):
	def __init__(self, ID):
		self.ID = ID
		self.programs = traci.trafficlight.getAllProgramLogics(self.ID)
		self.default_program = self.getTlProgram('default')
		self.lastChange = traci.simulation.getTime()
		self.prog_dict = self.genEdge2Prog()
		self.INC = 0
		self.ALT = 0
		self.alt_program = None
		self.ALT_ON = False
		self.t_switch = -1
		
		self.SWITCH_TIME = SWITCH_TIME
		self.MIN_SIGNAL_TIME = MIN_SIGNAL_TIME
		
		self.desired = 0
		# 0-normal program desired
		# 1-alt program desired
		
		self.state = 0	
		# 0-normal program
		# 1-switching to alt program
		# 2-start alt program 
		# 3-alt program 
		# 4-switching to normal program
		# 5-start normal program
		
	def getTlProgram(self,progID):
		ret = None
		for prog in self.programs:
			if prog.programID == progID:
				ret = prog
				break
		return ret
		
	def getLaneIDs(self):
		return traci.trafficlight.getControlledLanes(self.ID)
		
	def getLaneState(self, laneID, state=None):
		if state == None:
			state=traci.trafficlight.getRedYellowGreenState(self.ID)
		laneIDs = self.getLaneIDs()
		for n in range(len(laneIDs)):
			if laneIDs[n] == laneID:
				return state[n]
		return None
		
	def genEdge2Prog(self):
		progs = dict()
		progs["wsep2c"] = 'west'
		progs["nsep2c"] = "north"
		progs["esep2c"] = "east"
		progs["ssep2c"] = "south"
		return progs
		
	def injectProgram(self,edge):
		self.alt_program = self.getTlProgram(self.prog_dict[edge])
		
	def switch2default(self):
		self.ALT_ON = False
		
	def switch2alt(self):
		self.ALT_ON = True
		
	def setSwitchTime(self, t_switch):
		self.t_switch = t_switch
		
	def incrementState(self):
		self.state = (self.state + 1)%6
		
	def isRed(self, state):
		ret = False
		if state in ['r', 'R']:
			ret = True
		return ret
	
	def isGreen(self, state):
		ret = False
		if state in ['g', 'G']:
			ret = True
		return ret	
		
	def isYellow(self, state):
		ret = False
		if state in ['y', 'Y']:
			ret = True
		return ret
		
	def transitionState(self, to_state, from_state=None):
		if from_state == None:
			state = traci.trafficlight.getRedYellowGreenState(self.ID)
		else:
			state = from_state
		ret = ""
		for n in range(len(state)):
			if self.isGreen(state[n]) and self.isRed(to_state[n]):
				ret += "y"
			else:
				ret += state[n]
		return ret
		
	def isSwitchState(self, inc):
		cur_duration = self.default_program.phases[inc].duration
				
		if cur_duration <= self.SWITCH_TIME:
			return True
		else:
			return False
	
	def canSwitch(self, inc, t, lastChange):
		lastChange = lastChange - t
		if self.isSwitchState(inc):
			return False
		else:
			min_duration = min(self.default_program.phases[inc].duration, self.MIN_SIGNAL_TIME)
			if lastChange + min_duration > 0:
				return True
			else:
				return False
		
	def earliestPreemption(self, inc, t, lastChange):
		ret = 0
		if self.canSwitch(inc, t, lastChange):
			ret = t + self.SWITCH_TIME
		elif self.isSwitchState(inc):
			ret = lastChange + self.default_program.phases[inc].duration + self.MIN_SIGNAL_TIME + self.SWITCH_TIME
		else:
			min_duration = min(self.default_program.phases[inc].duration, self.MIN_SIGNAL_TIME)
			ret = max(lastChange + min_duration, t) + self.SWITCH_TIME
		return ret
		
	
		
	def futureProg(self, tspan, laneID, k_switch=-1):
		if laneID==None:
			print("\nNO LANE SUPPLIED TO GENERATE FUTURE PLAN ! \n")
			return None
		
		f_prog = []
		state = 0
		lastChange = self.lastChange - traci.simulation.getTime()
		inc = self.INC
		
		edgeID = traci.lane.getEdgeID(laneID)
		
		alt_prog = self.getTlProgram(self.prog_dict[edgeID])
		
		signal = self.getLaneState(laneID,self.default_program.phases[inc].state)
		
		for k in range(tspan.size):
			t = tspan[k]
			
			# if k == k_switch:
				# state += 1
			
			#check whether a switch needs to occur now
			if state == 0 and k_switch >= 0:
				if (t - lastChange) >= self.default_program.phases[inc].duration:
					next_switch = self.earliestPreemption((inc+1)%len(self.default_program.phases), t, t)
				else:
					next_switch = self.earliestPreemption(inc, t, lastChange)
				if next_switch >= tspan[k_switch]:
					state += 1
		
			# STANDARD TF PLAN --->
			if state == 0 and (t - lastChange) >= self.default_program.phases[inc].duration:
					inc = (inc+1)%len(self.default_program.phases)
					lastChange = t
					signal = self.getLaneState(laneID,self.default_program.phases[inc].state)
			# <--- STANDARD TF PLAN
		
			
			
		
			# SWITCHING TO ALT --->
			if state == 1 and (t - lastChange) >= MIN_SIGNAL_TIME:
				lastChange = t
				switch_state = self.transitionState(alt_prog.phases[0].state, self.default_program.phases[inc].state)
				signal = self.getLaneState(laneID,switch_state)
				state += 1
			# <--- SWITCHING TO ALT
		
			# START ALT --->
			if state == 2 and (t - lastChange) >= SWITCH_TIME:
				lastChange = t
				signal = self.getLaneState(laneID,alt_prog.phases[0].state)
				state += 1
			# <--- START ALT
			
			f_prog.append(signal)
		
		return f_prog
				 
	def step(self, t):
		if t == 0:
			t = traci.simulation.getTime()
			
		#check whether a switch needs to occur now
		if self.state == 0 and self.t_switch >= 0:
			if (t - self.lastChange) >= self.default_program.phases[self.INC].duration:
				next_switch = self.earliestPreemption((self.INC+1)%len(self.default_program.phases), t, t)
			else:
				next_switch = self.earliestPreemption(self.INC, t, self.lastChange)
			if next_switch >= self.t_switch:
				self.switch2alt()
			
		# STANDARD TF PLAN --->
		if self.state == 0:
			if self.ALT_ON:
				self.incrementState()
				self.t_switch = -1
			elif (t - self.lastChange) >= self.default_program.phases[self.INC].duration:
				self.INC = (self.INC+1)%len(self.default_program.phases)
				self.lastChange = t
				traci.trafficlight.setRedYellowGreenState(self.ID,self.default_program.phases[self.INC].state)
		# <--- STANDARD TF PLAN
		
		# SWITCHING TO ALT --->
		if self.state == 1 and (t - self.lastChange) >= MIN_SIGNAL_TIME:
			self.lastChange = t
			switch_state = self.transitionState(self.alt_program.phases[0].state)
			traci.trafficlight.setRedYellowGreenState(self.ID,switch_state)
			self.incrementState()
		# <--- SWITCHING TO ALT
		
		# START ALT --->
		if self.state == 2 and (t - self.lastChange) >= SWITCH_TIME:
			self.ALT = 0
			self.lastChange = t
			traci.trafficlight.setRedYellowGreenState(self.ID,self.alt_program.phases[self.ALT].state)
			self.incrementState()
		# <--- START ALT
		
		# ALT TF PLAN --->
		if self.state == 3:
			if not self.ALT_ON:
				self.incrementState()
			elif(t - self.lastChange) >= self.alt_program.phases[self.ALT].duration:
				self.lastChange = t
				self.ALT = (self.ALT+1)%len(self.alt_program.phases)
				traci.trafficlight.setRedYellowGreenState(self.ID,self.alt_program.phases[self.ALT].state)
		# <--- ALT TF PLAN
		
		# SWITCHING TO DEFAULT --->
		if self.state == 4 and (t - self.lastChange) >= MIN_SIGNAL_TIME:
			self.lastChange = t
			self.INC = (self.INC+1)%len(self.default_program.phases)
			switch_state = self.transitionState(self.default_program.phases[self.INC].state)
			traci.trafficlight.setRedYellowGreenState(self.ID,switch_state)
			self.incrementState()
		# <--- SWITCHING TO DEFAULT
		
		# START ALT --->
		if self.state == 5 and (t - self.lastChange) >= SWITCH_TIME:
			self.lastChange = t
			traci.trafficlight.setRedYellowGreenState(self.ID,self.default_program.phases[self.INC].state)
			self.incrementState()
		# <--- START ALT
		
		return True
				
				
				
				
				
