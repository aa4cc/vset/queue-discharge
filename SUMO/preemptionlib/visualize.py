import matplotlib as mpl
mpl.rcParams['toolbar'] = 'None' 
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np

plt.ion()

SCREEN_WIDTH = 14.82	# width in inches
SCREEN_HEIGHT = 8.33	# height in inches
SCREEN_DPI = 129.58		# DPI

LINEWIDTH = 2
COLS = ['#254fd9', '#d9d025', '#d9253a']

SAVE_DIR = "../figures"


def draw_plan(ax, t, light, y = 0, height=1, alp = 0.5):
	col = 'none'
	for k in range(t.size-1):
		if light[k] == 'G' or light[k] == 'g':
			col = 'green'
		elif light[k] == 'Y' or light[k] == 'y':
			col = 'yellow'
		elif light[k] == 'R' or light[k] == 'r':
			col = 'red'
		else:
			col = 'none'
		ax.add_patch(Rectangle((t[k],y), t[k+1]-t[k], height, edgecolor='none', facecolor=col, lw=0, alpha=alp))
		ax.figure.canvas.draw_idle()
		
def draw_cost(axes, t, cost):
	
	axes[0].set_title(r'Cost function elements')
	axes[0].plot(t, cost.transpose())
	axes[0].legend([r'$C_1$', r'$C_2$'])
	axes[0].grid()
	
	axes[1].set_title(r'Cost function')
	axes[1].plot(t, cost.sum(axis=0))
	axes[1].grid()
	axes[1].set_ylabel(r'$C$')
	axes[1].set_xlabel(r'$t \, \mathrm{[s]}$')
	
	axes[0].figure.canvas.draw_idle()
	
def draw_sim(axes, ts, xs, vs, q_len = None, t_switch = -1):
	if q_len == None:
		q_len = xs.shape[0]
	else:
		q_len = min(xs.shape[0], q_len)
	
	if q_len > 0:
		axes[0].plot(ts, xs[:q_len,:].transpose(), lw=LINEWIDTH, c=COLS[0], ls='-')
		axes[1].plot(ts, vs[:q_len,:].transpose(), lw=LINEWIDTH, c=COLS[0], ls='-')
	
	if q_len < xs.shape[0]-1:
		axes[0].plot(ts, xs[q_len:-1,:].transpose(), lw=LINEWIDTH, c=COLS[1], ls='--')
		axes[1].plot(ts, vs[q_len:-1,:].transpose(), lw=LINEWIDTH, c=COLS[1], ls='--')
	if q_len <= xs.shape[0]-1:
		axes[0].plot(ts, xs[-1,:].transpose(), lw=LINEWIDTH, c=COLS[2], ls='-')
		axes[1].plot(ts, vs[-1,:].transpose(), lw=LINEWIDTH, c=COLS[2], ls='-')
		
	if t_switch	>= 0:
		axes[0].plot(np.array((t_switch, t_switch)), np.array((xs.min(), xs.max())), lw=LINEWIDTH, c="black", ls='--', alpha=0.5)
		axes[1].plot(np.array((t_switch, t_switch)), np.array((vs.min(), vs.max())), lw=LINEWIDTH, c="black", ls='--', alpha=0.5)

	axes[0].set_ylabel(r'$x \, \mathrm{[m]}$')
	axes[1].set_ylabel(r'$v \, \mathrm{[ms^{-1}]}$')
	axes[1].set_xlabel(r'$t \, \mathrm{[s]}$')
	axes[0].grid()
	axes[1].grid()
	axes[0].figure.canvas.draw_idle()
		
def add_formating(axes):
	axes[0].set_ylabel(r'$J$')
	#axes[0].set_xlabel(r'$t \, [s]$')
	
	axes[1].set_ylabel(r'$x \, [m]$')
	#axes[1].set_xlabel(r'$t \, [s]$')
	
	axes[2].set_ylabel(r'$v \, [ms^{-1}]$')
	#axes[2].set_xlabel(r'$t \, [s]$')
	
	axes[3].set_ylabel(r'$t_{\mathrm{+}} \, [s]$')
	#axes[3].set_xlabel(r'$t_{\mathrm{signal}} \, [s]$')
	axes[3].set_xlabel(r'$t \, [s]$')
	
class BoxChart():
	def __init__(self, labels=None, title=None, xlabel=None, ylabel = r"$t \, \left[ \mathrm{s} \right]$"):
		self.fig, self.ax = plt.subplots(figsize=(SCREEN_WIDTH/2.5,SCREEN_HEIGHT/3), dpi=SCREEN_DPI, tight_layout=True)
		self.labels = labels
		self.title = title
		self.xlabel = xlabel
		self.ylabel = ylabel
		
	def updateChart(self, data):
		self.ax.clear()
		out = self.ax.boxplot(data, notch=False, labels=self.labels)
		self.ax.set_title(self.title)
		self.ax.set_ylabel(self.ylabel)
		self.ax.set_xlabel(self.xlabel)
		self.ax.grid()
		self.ax.figure.canvas.draw_idle()
		self.fig.canvas.flush_events()
		self.fig.show()
		
		ret = []
		for r in out['medians']:
			ret.append(r.get_ydata()[0])
		
		return ret
		
	def save(self, name, folder=SAVE_DIR):
		self.fig.savefig(folder+"/" + name+".pdf", format='pdf', dpi=1200, trasparent=True, bbox_inches='tight', pad_inches=0)
		
	def close(self):
		plt.close(fig=self.fig)
		
class ProgressBar():
	def __init__(self, color='blue'):
		self.color=color
		self.fig, self.ax = plt.subplots(figsize=(SCREEN_WIDTH/3,SCREEN_HEIGHT*0.1), dpi=SCREEN_DPI, tight_layout=True, frameon=False)
		
	def updateBar(self, progress, time = -1):
		self.ax.clear()
		self.ax.add_patch(Rectangle((0,0), progress, 1, edgecolor='none', facecolor=self.color, lw=0, alpha=0.5))
		self.ax.add_patch(Rectangle((0,0), progress, 1, edgecolor='black', facecolor='none', lw=1))
		
		if time >0:
			h = np.floor(time/3600)
			m = np.floor((time - h*3600)/60)
			s = np.floor(time - h*3600 - m*60)
		
			time_text = "ETC: %dh:%dm:%ds" %(h,m,s)
			self.ax.text(0.5, 0.5, time_text, fontsize=25, horizontalalignment='center', verticalalignment='center', transform=self.ax.transAxes)
		
		self.ax.set_yticks([])
		self.ax.set_xticks([])
		
		self.ax.figure.canvas.draw_idle()
		self.fig.canvas.flush_events()
		
	def save(self, name, folder=SAVE_DIR):
		self.fig.savefig(folder+"/" + name+".pdf", format='pdf', dpi=1200, trasparent=True, bbox_inches='tight', pad_inches=0)
		
	def close(self):
		plt.close(fig=self.fig)
		
		
	
		
		
		
		
		
		
