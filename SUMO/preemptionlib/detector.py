import traci
from preemptionlib.models import Parameters

class Detector(traci.StepListener):
	def __init__(self, detID):
		self.detID = detID
		self.laneID = traci.lanearea.getLaneID(self.detID)
		self.laneIndex = int(self.laneID[-1])
		self.edgeID = traci.lane.getEdgeID(self.laneID)
		self.pos = traci.lanearea.getPosition(self.detID)
		self.len = traci.lanearea.getLength(self.detID)
		self.veh_num = 0
		self.veh_ids = []
		self.veh_x = []
		self.veh_v = []
		
	def getParams(self, model):
		params = []
		for vhcl in self.veh_ids:
			params.append(Parameters(vhcl, model))
		return params
		
	def getVehIDS(self):
		# returnd IDs of vehicles currently detected
		ids = traci.lanearea.getLastStepVehicleIDs(self.detID)
		return ids
		
	def getDistance(self, vehID):
		return traci.vehicle.getDrivingDistance(vehID, self.edgeID, self.pos + self.len, self.laneIndex)
	
	def step(self, t):
		# generates sorted IDs, positions and velocities of detected vehicles
		# the closest vehicle to intersection is first
		ids_in = self.getVehIDS()
		
		pos = []
		vel = []
		ids_out = []
		for vehID in ids_in:
			x = -self.getDistance(vehID)
			
			if x > 10:
				continue
			
			v = traci.vehicle.getSpeed(vehID)
			pos.append(x)
			vel.append(v)
			ids_out.append(vehID)
			
			
		if ids_out:
			self.veh_ids, self.veh_x, self.veh_v = (list(t) for t in zip(*sorted(zip(ids_out, pos, vel), key=lambda x: x[1], reverse=True)))
		self.veh_num = len(ids_out)
			
		return True
			
	
