import traci
from preemptionlib.estimator import Estimator
from preemptionlib.detector import Detector
from preemptionlib.ambulance import Ambulance
from preemptionlib.traffic_light import TrafficLight
import numpy as np


class Scheduler(traci.StepListener):
	def __init__(self, intersection ,mode="prune", probabilities=None, radius = 500, verbose = False, visualize=False):
		self.intersection = intersection
		self.mode = mode
		self.radius = radius
		self.VERBOSE = verbose
		self.VISUALIZE = visualize
		
		self.dets = self.getDets();			
		
		self.tl = TrafficLight(intersection)
		traci.addStepListener(self.tl)
		
		self.probs=probabilities
		
		self.est = Estimator(mode, self.tl, probs=self.probs, verbose=self.VERBOSE, visualize=self.VISUALIZE)
		
		self.t_switch = 0
		self.ambs = []
		
		self.stall_vehs = []
		self.stall_t = 0
		self.stall_n = 0
		self.avg_stall = 0
		
		self.amb_stall = 0
		
		self.amb_min_vel = np.inf
		
		self.amb_signal = False
		
		self.recompute = False
		if mode=="recomp":
			self.recompute = True
		self.recomp_last = 0
		self.recomp_again = 1
		
		
	
	def getDets(self):
		# returns dictionary of (detector ID, detector instance) pairs
		detIDs = traci.lanearea.getIDList()
		dets = dict()
		for detID in detIDs:
			dets[detID] = Detector(detID)
			traci.addStepListener(dets[detID])
		return dets
		
	def scanAmbs(self):
		# Checks if any ambulances appeared and adds them to the list
		# then sorts existing ambulances based on distance from intersection
		# then deletes any passed ambulances
		# returns (bool) found, (bool) lost
		
		found = False
		IDs = traci.simulation.getDepartedIDList()
		for vehID in IDs:
			if traci.vehicle.getVehicleClass(vehID) == "emergency":
				amb = Ambulance(vehID, self.dets)
				self.ambs.append(amb)
				found = True
		
		for i in range(len(self.ambs)):
			self.ambs[i].step()
		self.ambs = sorted(self.ambs)
		
		gone = True
		lost = False
		while gone and len(self.ambs) > 0:
			if self.ambs[0].gone:
				del self.ambs[0]
				lost = True
			else:
				gone = False
			
		return found, lost
		
		
	def vhclInbound(self,ID):
		ret = False
		nextTLS = traci.vehicle.getNextTLS(ID)
		if len(nextTLS) > 0:
			if nextTLS[0][0] == self.intersection:
				ret = True
		return ret
		
	def ambSignIn(self):
		ret = False
		if self.amb_signal == False and len(self.ambs) > 0:
			if self.ambs[0].distance <= self.radius:
				ret = True
				self.amb_signal = True
		return ret
	
	def step(self, t):
		if t == 0:
			t = traci.simulation.getTime()
			
		if self.VERBOSE: 
			print("\rt = %.2f" %t, end='')
			
		
		# does one step of the scheduling algorithm
		
		# AMBULANCES --->
		found, lost = self.scanAmbs()
		if lost:
			self.amb_signal = False
		sign_in = self.ambSignIn()
		# <--- AMBULANCES
		
		# VEHICLES --->
		if sign_in:
			for vehID in traci.vehicle.getIDList():
				if traci.vehicle.getVehicleClass(vehID) != "emergency" and (not (vehID in self.stall_vehs)) and self.vhclInbound(vehID):
					self.stall_vehs.append(vehID)
			if self.VERBOSE: 
				print(", %s signed-in" %(self.ambs[0].ID), end='')
		
		if self.amb_signal and (not sign_in):
			# add every new car except ambulances
			for vehID in traci.simulation.getDepartedIDList():
				if traci.vehicle.getVehicleClass(vehID) != "emergency" and (not (vehID in self.stall_vehs)) and self.vhclInbound(vehID):
					self.stall_vehs.append(vehID)
			self.amb_stall = traci.vehicle.getAccumulatedWaitingTime(self.ambs[0].ID)
			self.amb_min_vel = self.ambs[0].minVelocity
			if self.VERBOSE: 
				print(", %s: dist= %.2f m, stall = %.2f s" %(self.ambs[0].ID, self.ambs[0].distance, self.amb_stall), end='')
				
			
					
		for vehID in self.stall_vehs:	
			if not self.vhclInbound(vehID):
				self.stall_t += traci.vehicle.getAccumulatedWaitingTime(vehID)
				self.stall_n += 1
				del self.stall_vehs[self.stall_vehs.index(vehID)]
				self.avg_stall = self.stall_t/self.stall_n
		if self.VERBOSE: 
			print(", %d affected vehicles, avg. stall=%.2fs" %(len(self.stall_vehs), self.avg_stall), end='')
			
		# <--- VEHICLES
		
		# ESTIMATOR --->
		if sign_in or (self.amb_signal and self.recompute and t>=self.recomp_last + self.recomp_again and not self.tl.ALT_ON):
			if self.recompute:
				self.recomp_last=t
			self.t_switch = self.est.estimate(self.ambs[0]) + t
			if self.VERBOSE: 
				print(", preemption time: %.2f, %d virtual vehicles" %(self.t_switch, self.est.n_virt), end='')
			self.tl.injectProgram(self.ambs[0].det.edgeID)
		# <--- ESTIMATOR
		
		# TRAFFIC LIGHT --->
		if lost:
			self.tl.switch2default()
			if self.VERBOSE: 
				print(", switching to default plan", end='')
		if self.amb_signal and not self.tl.ALT_ON:
			self.tl.setSwitchTime(self.t_switch)
			if self.VERBOSE: 
				print(", scheduling preemption at %.2f" %self.t_switch, end='')
		# <--- TRAFFIC LIGHT
		
		if self.VERBOSE: 
			print("\n", end='')
		
		return True
		
		
	
				
			
			
					
	
	
