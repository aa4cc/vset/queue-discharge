import traci
from preemptionlib.detector import Detector
from preemptionlib.models import Model, Parameters, Probabilities
import matplotlib.pyplot as plt
import numpy as np
from math import ceil
import random
from preemptionlib.visualize import *
from time import time


class Estimator():
	def __init__(self, mode, tl, probs=None, verbose = False, visualize=False):
		self.mode = mode
		self.VERBOSE = verbose
		self.VISUALIZE = visualize
		self.tl = tl
		self.model = Model("IIDM")
		self.probs = probs
		self.n_virt = 0
		
		if self.VISUALIZE:
			self.sim_fig, self.sim_axes = plt.subplots(2, 1, figsize=(SCREEN_WIDTH/2,SCREEN_HEIGHT*0.9), dpi=SCREEN_DPI, sharex='all', tight_layout=True)
			
			if mode == "prune":
				self.cost_fig, self.cost_axes = plt.subplots(2, 1, figsize=(SCREEN_WIDTH/2,SCREEN_HEIGHT*0.9), dpi=SCREEN_DPI, sharex='all', tight_layout=True)

	def estimate(self, amb):
		# Estimates optimal switching delay
		if self.mode == "radius":
			ret = 0;
		elif self.mode == "preemption" or self.mode == "recomp":
			ret = self.preemption_time(amb)
		elif self.mode == "prune":
			ret = self.prune_cost_function(amb)
		else:
			ret = np.inf
		return ret
		
	def sameStates(self,s1,s2):
		ret = False
		green = ['g','G']
		yellow = ['y','Y']
		red = ['r','R']
		cols = [green, yellow, red]
		for col in cols:
			if s1 in col and s2 in col:
				ret = True
		return ret
		

	def buildDefaultProg(self, tspan, tl, laneID):
		now_phase = tl.INC
		last_change = tl.lastChange - traci.simulation.getTime()
		next_change = last_change + tl.default_program.phases[now_phase].duration
		state = tl.getLaneState(laneID,tl.default_program.phases[now_phase].state)
		
		dt = self.model.dt
		ret = ['R']*tspan.size
		
		# default program
		for k in range(tspan.size):
			if next_change <= tspan[k]:
				now_phase = (now_phase + 1)%len(tl.default_program.phases)
				last_change = next_change
				next_change = tl.default_program.phases[now_phase].duration + tspan[k]
				state = tl.getLaneState(laneID,tl.default_program.phases[now_phase].state)
			ret[k] = state
		return ret
 
	def buildAltProg(self, tspan, switch, tl, laneID):
		now_phase = tl.INC
		last_change = tl.lastChange - traci.simulation.getTime()
		next_change = last_change + tl.default_program.phases[now_phase].duration
		state = tl.getLaneState(laneID,tl.default_program.phases[now_phase].state)
		
		dt = self.model.dt
		ret = ['R']*tspan.size
		
		last_k = 0
		
		# default program until switching
		for k in range(tspan.size):
			if next_change <= tspan[k]:
				now_phase = (now_phase + 1)%len(tl.default_program.phases)
				old_state = state
				state = tl.getLaneState(laneID,tl.default_program.phases[now_phase].state)
				last_change = next_change
				next_change = tl.default_program.phases[now_phase].duration + tspan[k]
			last_k = k
			if tspan[k] >= switch and tspan[k] >= last_change + tl.MIN_SIGNAL_TIME:
				break
			ret[k] = state
				
		# green state for the rest of the program
		for k in range(last_k, tspan.size):
			ret[k] = 'G'
				
		
		return ret
		
	def early_switch_cost(self, max_t, switch_t):
		es_cost = 1-switch_t/max_t
		return es_cost
		
	def amb_decel_cost(self, best_v, v):
		ad_cost = 1-v/best_v
		return ad_cost
	
	def build_sim(self, amb, probs = None):
		
		# measure amb pos/vel
		amb_dist = amb.distance
		amb_vel = amb.velocity
		
		# initialization
		params = Parameters("INIT", "INIT_IIDM")
		dl = 0
		dm = 0
		
		# REAL VEHICLES--->
		n_real = np.sum(np.array(amb.det.veh_x) > -amb_dist)
		p_real = []
		x0_real = np.empty((2,n_real))
		for vhcl in range(n_real):
			params = Parameters(amb.det.veh_ids[vhcl], "IIDM")
			dm = params.l
			params.l = dl
			p_real.append(params)
			x0_real[0,vhcl] = amb.det.veh_x[vhcl]
			x0_real[1,vhcl] = amb.det.veh_v[vhcl]
			dl = dm
		
		# <---REAL VEHICLES
		
		# VIRTUAL VEHICLES--->
		n_virt = 0
		p_virt = []
		x0_virt = np.empty((2,0))
		if probs != None and amb_dist > amb.det.len:
			secs2det = int(ceil((amb_dist-amb.det.len)/amb_vel))
			types = len(probs.vehicles)
			count = np.zeros((types,))
			for k in range(types):
				prob = probs.vehProb(probs.vehicles[k],amb.source)
				count[k] = round(ceil(prob*secs2det))
			n_virt = int(round(count.sum()))
			
			sep_dist = (amb_dist - amb.det.len)/(n_virt+1)
			x0_virt = np.zeros((2,n_virt))
			k = 0
			vehs = []
			while np.any(count > 0):
				if count[k] > 0:
					vehs.append(probs.vehicles[k])
					count[k] -= 1
				k = (k+1)%types
			
			last_v = params.V0
			if n_real > 0:
				last_v = min(x0_real[1,-1], last_v)
			
			virt_vels = np.linspace(last_v, min(params.V0, amb_vel), len(vehs)+2)
			virt_vels = virt_vels[1:-1]
			
			v_des = params.V0
			for vhcl in range(n_virt):
				params = Parameters("virt_%d_%s" %(vhcl, vehs[vhcl]), "VIRT_IIDM")
				params.V0 = v_des
				params.a = traci.vehicletype.getAccel(vehs[vhcl])
				params.b = traci.vehicletype.getDecel(vehs[vhcl])
				params.T = traci.vehicletype.getAccel(vehs[vhcl])
				params.s0 = traci.vehicletype.getMinGap(vehs[vhcl])
				dm = traci.vehicletype.getLength(vehs[vhcl])
				params.l = dl
				p_virt.append(params)
				x0_virt[0, vhcl] = -amb.det.len - (vhcl+1)*sep_dist + dm/2
				x0_virt[1, vhcl] = virt_vels[vhcl]
				dl = dm
				
		self.n_virt = n_virt
		# <--- VIRTUAL VEHICLES
	
		# EMERGENCY VEHICLE--->
		params = Parameters(amb.ID, "IIDM")
		dm = params.l
		params.l = dl
		# <---EMERGENCY VEHICLE		
		
		
		
		# OTUPUT--->
		x0 = np.hstack([x0_real, x0_virt])
		p = p_real
		p.extend(p_virt)
		
		# make sure amb is not overtaking last vehicle and that vortual vehicles fit
		if n_real + n_virt > 0:
			for k in range(max(1, n_virt)):
				if -amb_dist + params.T*amb_vel + params.s0 > x0[0,-1]:
					x0 = x0[:,:-1]
					p = p[:-1]
		
		
		x0 = np.hstack([x0, np.array([[-amb_dist],[amb_vel]])])
		
		
		p.append(params)
		
		return x0, p

	def prune_cost_function(self, amb, dt=1):
		
		
		tic = time()
		
		# find simulation time
		v_des = traci.vehicle.getAllowedSpeed(amb.ID)
		tmax = amb.distance/v_des
		sim_span = np.arange(0,2*tmax+self.model.dt,self.model.dt)
		
		opt_span = np.arange(0,2*tmax+dt,dt)
		
		cost = np.empty((2, opt_span.size))		# 0 - absolute variaton of decel
												# 1 - early switch cost
												# 2 - time of passage
		# traffic light plan
		def_prog = self.tl.futureProg(sim_span, amb.det.laneID)
		
		x0, pars = self.build_sim(amb, self.probs)
		
		opt_k = 0
		
		# run one simulation as if the switch happened now
		alt_prog = self.tl.futureProg(sim_span, amb.det.laneID, 0)
		xs, vs, ts = self.model.discharge(x0, sim_span, pars, alt_prog)
		best_slow_v = vs[-1,:].min()
		
		for k in range(opt_span.size):
			
			prog_k = np.sum(sim_span<opt_span[k])
			
			
			alt_prog = self.tl.futureProg(sim_span, amb.det.laneID,  prog_k)
			xs, vs, ts = self.model.discharge(x0, sim_span, pars, alt_prog)
			
			decel_cost = self.amb_decel_cost(best_slow_v, vs[-1,:].min())
			early_cost = self.early_switch_cost(tmax, opt_span[k])
			
			cost[0,k] = (1-amb.priority)*early_cost
			cost[1,k] = amb.priority*decel_cost
			
			
			if cost[0,k] + cost[1,k] <= cost[0,opt_k] + cost[1,opt_k]:
				opt_k = k
				
			if self.VISUALIZE:
				# show simulation:
				if amb.distance > amb.det.len:
					n_real = len(amb.det.veh_x)
				else:
					n_real = xs[:,0].size - 1
				
				
				draw_sim(self.sim_axes, ts, xs, vs,  n_real, t_switch=opt_span[k])
				x_width = (xs.max()-xs.min())*0.05
				v_width = (vs.max()-vs.min())*0.05
				#draw_plan(self.sim_axes[0], ts, def_prog[:ts.size], y=max(xs.max()+0.1*x_width, 1.1*x_width), height=x_width, alp=1)
				draw_plan(self.sim_axes[0], ts, alt_prog[:ts.size], y=0, height=x_width, alp=0.4)
				draw_plan(self.sim_axes[1], ts, alt_prog[:ts.size], y=vs.min()-v_width, height=v_width, alp=0.4)
				
				#show cost:
				draw_cost(self.cost_axes, opt_span[:k+1], cost[:,:k+1])
				
				self.sim_fig.canvas.flush_events()
				self.cost_fig.canvas.flush_events()
				
				sometext = input("\n Do you want to save figure? Name (min 4 chars, less means no): ")
				if len(sometext) > 4:
					self.sim_fig.savefig("figures/%s_sim.pdf" %sometext, format='pdf', dpi=1200, trasparent=True, bbox_inches='tight', pad_inches=0)
					self.cost_fig.savefig("figures/%s_cost.pdf" %sometext, format='pdf', dpi=1200, trasparent=True, bbox_inches='tight', pad_inches=0)
				
				for sim_ax in self.sim_axes:
					sim_ax.clear()
				
				for cost_ax in self.cost_axes:
					cost_ax.clear()
					
				
				
			
			if ts[-1] < opt_span[k]:
				break
				
		if self.VERBOSE:		
			print(", comp: t=%.2fs" %(time()-tic), end='')
				
		return opt_span[opt_k]
		
	

	def preemption_time(self, amb):
		tic = time()
		
		# find simulation time
		v_des = traci.vehicle.getAllowedSpeed(amb.ID)
		
		tmax = amb.distance/v_des
		sim_span = np.arange(0,2*tmax+self.model.dt,self.model.dt)
		# traffic light plan
		def_prog = self.tl.futureProg(sim_span, amb.det.laneID)
		
		x0, pars = self.build_sim(amb, self.probs)
		
		
		# run one simulation as if the switch happened now
		alt_prog = self.tl.futureProg(sim_span, amb.det.laneID, 0)
		xs, vs, ts = self.model.discharge(x0, sim_span, pars, alt_prog)
		
		# run one simulation without queue
		x_empty, v_empty, t_empty = self.model.discharge(x0[:,-1].reshape((2,1)), sim_span, [pars[-1]], alt_prog)
		
		tmax = t_empty[-1]
		
		# minimization params
		best_slow_v = vs[-1,:].min()
		left = 0
		right = min(ts.size, np.sum(ts<=tmax))
		iters = 0
		
		left_new = True
		right_new = True
		best_k = 0
		
		
		while abs(left - right) > 1:
			if left_new:
				l_prog = self.tl.futureProg(sim_span, amb.det.laneID, left)
				l_x, l_v, l_t = self.model.discharge(x0, sim_span, pars, l_prog)
				l_decel_cost = self.amb_decel_cost(best_slow_v, l_v[-1,:].min())
				l_early_cost = self.early_switch_cost(tmax, sim_span[left])
				l_cost = (1-amb.priority)*l_early_cost + amb.priority*l_decel_cost
				left_new = False
				
				if self.VISUALIZE:
					# show simulation:
					draw_sim(self.sim_axes, l_t, l_x, l_v, np.sum(np.array(amb.det.veh_x) > -amb.distance), t_switch=sim_span[left])
					
					x_width = (l_x.max()-l_x.min())*0.05
					v_width = (l_v.max()-l_v.min())*0.05
					draw_plan(self.sim_axes[0], l_t[:ts.size], def_prog[:ts.size], y=max(l_x.max()+0.1*x_width, 1.1*x_width), height=x_width, alp=1)
					draw_plan(self.sim_axes[0], l_t[:ts.size], l_prog[:ts.size], y=0, height=x_width, alp=0.4)
					draw_plan(self.sim_axes[1], l_t[:ts.size], l_prog[:ts.size], y=l_v.min(), height=v_width, alp=0.4)
					
					self.sim_fig.canvas.flush_events()
					for sim_ax in self.sim_axes:
						sim_ax.clear()
				
			if right_new:
				r_prog = self.tl.futureProg(sim_span, amb.det.laneID, right)
				r_x, r_v, r_t = self.model.discharge(x0, sim_span, pars, r_prog)
				r_decel_cost = self.amb_decel_cost(best_slow_v, r_v[-1,:].min())
				r_early_cost = self.early_switch_cost(tmax, sim_span[right])
				r_cost = (1-amb.priority)*r_early_cost + amb.priority*r_decel_cost
				right_new = False
				
			if r_cost > l_cost:
				right = int(round((left+right)/2))
			else:
				dist = right-left
				left = right
				l_cost = r_cost
				right = min(right+dist, np.sum(ts<=tmax))
				
				
				if self.VISUALIZE:
					# show simulation:
					draw_sim(self.sim_axes, r_t, r_x, r_v, np.sum(np.array(amb.det.veh_x) > -amb.distance), t_switch=sim_span[right])
					
					x_width = (r_x.max()-r_x.min())*0.05
					v_width = (r_v.max()-r_v.min())*0.05
					draw_plan(self.sim_axes[0], r_t[:ts.size], def_prog[:ts.size], y=max(r_x.max()+0.1*x_width, 1.1*x_width), height=x_width, alp=1)
					draw_plan(self.sim_axes[0], r_t[:ts.size], r_prog[:ts.size], y=0, height=x_width, alp=0.4)
					draw_plan(self.sim_axes[1], r_t[:ts.size], r_prog[:ts.size], y=r_v.min(), height=v_width, alp=0.4)
					
					self.sim_fig.canvas.flush_events()
					for sim_ax in self.sim_axes:
						sim_ax.clear()
				
				
			right_new = True

			iters += 1
				
			
			
		if self.VERBOSE:
			print(", comp: t=%.2fs in %d steps" %(time()-tic, iters), end='')
				
		return sim_span[left]
		
		
		
		
		
		
		
	
	

