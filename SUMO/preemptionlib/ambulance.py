import traci


class Ambulance(traci.StepListener):
	def __init__ (self, ID, dets, priority = None):
		self.ID = ID
		self.detID = self.findDetID()
		self.det = dets[self.detID]
		self.distance = self.getDistance2det()
		self.velocity = self.getVelocity()
		self.minVelocity = self.velocity
		self.source = self.getSource()
		self.sink = self.getSink()
		self.gone = False
		if priority == None:
			self.priority = 0.9
		else:
			self.priority = priority
		
	def __lt__(self, other):
		# comparisson (<) override
		# self if closer than other to intersection -> self<other
		return self.distance < other.distance
	
	def __eq__(self, other):
		# equality (==) override
		return self.distance == other.distance
		
	def assignDetLane2Amb(self):
		# finds lane of a corresponding detector
		edgeIDs = traci.vehicle.getRoute(self.ID)	# IDs of edges in Amb's path
		num_lanes = traci.edge.getLaneNumber(edgeIDs[1])
		for lane_num in range(1, num_lanes):
			for link in traci.lane.getLinks(edgeIDs[1]+'_{}'.format(lane_num)):
				if traci.lane.getEdgeID(link[0]) == edgeIDs[2]:
					return edgeIDs[1] + '_{}'.format(lane_num)
		return error('Emergency vehicle could not be assigned to any detector')
		
	def findDetID(self):
		# finds ID of a corresponding detector
		detLaneID = self.assignDetLane2Amb()
		detIDs = traci.lanearea.getIDList()
		for detID in detIDs:
			if traci.lanearea.getLaneID(detID) == detLaneID:
				return detID
		return error('Emergency vehicle\'s estimator could not be assigned to any detector')
		
	def getDistance2det(self):
		# finds distance to detector
		distance = traci.vehicle.getDrivingDistance(self.ID, self.det.edgeID, self.det.pos + self.det.len, self.det.laneIndex)
		if distance < 0:
			self.gone = True
		return distance
		
	def getSource(self):
		route = traci.vehicle.getRoute(self.ID)
		return route[0]
	
	def getSink(self):
		route = traci.vehicle.getRoute(self.ID)
		return route[-1]
		
	def getVelocity(self):
		vel = traci.vehicle.getSpeed(self.ID)
		return vel
		
	def step(self,t=0):
		self.distance = self.getDistance2det()
		self.velocity = self.getVelocity()
		self.minVelocity = min(self.velocity, self.minVelocity)
		if self.gone:
			ret = False
		else:
			ret = True
		return ret
