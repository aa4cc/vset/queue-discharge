# Modeling and optimization for traffic signal preemption for emergency vehicles using V2X communication

## Compilation
### Using MiKTex
- add [thesistexmf](./thesistexmf/) into your TEXMF root directories in MiKTex console settings

## Folders
### figures
- contains pictures used in the thesis 

### schematics
- contains various graphic files used for schematics. All fall under cc0 license.

### titlepage
- book cover front page design