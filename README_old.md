# Modeling of traffic queue discharge dynamics

![priority vehicle approaching](./pictures/visualize_control/I_2_L_17_Q_1_IIDM_VEL_EN.png "queue discharge optimization for a priority vehicle approaching an intersection")

## Intro

This project aims to find a suitable solution for estimation of the time it takes for a queue of vehicles at a traffic light intersection to cross, or reach a desired velocity.

## Methods used (so far)

* [Intelligent driver model](https://en.wikipedia.org/wiki/Intelligent_driver_model) (IDM)
* Improved intelligent driver model (source?)
* [Linear time-invariant MIMO system](https://en.wikipedia.org/wiki/Linear_time-invariant_system) (LTI)

## Directories and contents

* [code](./code) - MATLAB functions, variables and scripts
* [mgmt](./mgmt) - management documentation of the team project (mainly obsolete)
* [pictures](./pictures) - any interesting pictures found along the way
* [raw_data](./raw_data) - .csv files with raw extracted data and pictures with information from the extraction process
* [report](./report) - technical report for the team project (mainly obsolete)

## Usage

[code](./code) includes four main MATLAB scripts

* [visualize_lane.m](./code/visualize_lane.m)
* [visualize_model.m](./code/visualize_model.m)
* [optimize_params.m](./code/optimize_params.m)
* [visualize_control.m](./code/visualize_control.m)
* [visualize_cost.mlx](./code/visualize_cost.mlx)

### visualize_lane.m

![output of visualize_lane.m](./pictures/visualize_lane/I_2_L_17_POS.png "output of visualize_lane.m for INTERSECTION 2, LANE 17")

Shows all data from a single lane.

1. in [raw_data](./raw_data) choose an intersection and a lane from CR\<X\>\_WITH\_GATES_SNAPSHOT.png, where \<X\> is a number of the intersection.
2. edit the values for INTERSECTION and LANE at the begining of the file.
3. run the script

### visualize_model.m

![position from visualize_model.m](./pictures/visualize_model/I_2_L_17_Q_1_IIDM_SEP_POS_EN.png "position output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

![velocity from visualize_model.m](./pictures/visualize_model/I_2_L_17_Q_1_IIDM_SEP_VEL_EN.png "velocity output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

Runs a selected estimation model on initial positions from queues of a given lane.

1. run visualize_lane.m to make sure your chosen lane includes interesting data
2. make sure INTERSECTION and LANE are correctly selected
3. choose from supported models using the variable MODEL
4. run the script

### optimize_params.m

Finds optimal parameters of a model and saves them to a file for future use.

**script does not include any backup functionality, make sure you make a backup of previously found paramters**

1. run visualize_lane.m to make sure your chosen lane includes interesting data and select a queue you want to train the model on
2. make sure MODEL, INTERSECTION, LANE and QUEUE are correctly selected
3. run the script
4. run visualize_model.m to see the results

### visualize_control.m

![position from visualize_control.m](./pictures/visualize_control/I_2_L_17_Q_1_IIDM_POS_EN.png "position output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

![velocity from visualize_control.m](./pictures/visualize_control/I_2_L_17_Q_1_IIDM_VEL_EN.png "velocity output of visualize_model.m for INTERSECTION 2, LANE 17, with IIDM MODEL")

Finds optimal time of signal switching after a priority vehicle signals the intersection its approach at desired speed. The intersection starts dischaging the queue in waiting at this optimal time. This time minimizes the change in velocity of the priority vehicle and the time of crossing the intersection, it also produces a figure showing the cost function.

1. run visualize_model.m and make sure the parameters are identical in this script.
2. select initial velocity INIT\_V, initial distance INIT\_D, distance after the intersection we still track the priority vehicle DES\_D, and priority PRIORITY_WEIGHT  
3. run the script
4. you can see the optimal distance and time for traffic light switching in the resulting plot  
  
### visualize_cost.mlx

A MATLAB live script showing the behavior of the preemption cost function with varying inputs. 
    
![cost function from visualize_cost.mlx](./pictures/visualize_cost/P_EN.png "cost function for optimal signal switching time for various priorities P")






